#!/bin/sh

service redis-server start
cd /mnt/rails
nohup /bin/bash -l -c "npm run startdev" &
nohup /bin/bash -l -c "bundle exec sidekiq" &
/bin/bash -l -c "bundle exec rails s -p 3000 -b 0.0.0.0"