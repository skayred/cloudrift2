# README #

This is an implementation of microservice testing service, called Cloudrift

### How do I get set up? ###

This project is implemented using Ruby on Rails, and it makes it a little harder to set it up on Windows.
In order to make the project easy-to-try, we've implemented the whole infrastructure upon docker and docker-compose.

Easy start:

* docker-compose build
* docker-compose run web /bin/sh seeds.sh
* docker-compose up

### Contact ###

* dmitrii.savchenko [at] lut.fi