import { connect } from 'react-redux';
import { setPageState } from '../actions/PageStatus.es6';

import flowRight from 'lodash/function/flowRight';
import get from 'lodash/object/get';
import assign from 'lodash/object/assign';

import MainLayout from './MainLayout.es6';

const stateToProps = (state, props) => {

  return assign({
    pageState: get(state.pageState, '0', 'status')
  });
};

const actionsToProps = dispatch => ({
  setPageState: flowRight(dispatch, setPageState)
});

export default connect(stateToProps, actionsToProps)(MainLayout);
