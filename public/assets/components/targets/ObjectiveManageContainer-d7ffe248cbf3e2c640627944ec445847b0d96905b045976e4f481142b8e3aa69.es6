import { connect } from 'react-redux';
import { createTargetMethod, removeTargetMethod, saveMethodsOrder } from '../../actions/TargetMethodsAction.es6';
import { loadMethodsList } from '../../actions/TestingMethodActions.es6';
import { runTest, saveObjectiveParams } from '../../actions/TestAction.es6';
import ObjectiveManage from './ObjectiveManage.es6';
import flowRight from 'lodash/function/flowRight';
import get from 'lodash/object/get';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createTargetMethod: flowRight(dispatch, createTargetMethod),
  removeTargetMethod: flowRight(dispatch, removeTargetMethod),
  loadMethodsList: flowRight(dispatch, loadMethodsList),
  saveMethodsOrder: flowRight(dispatch, saveMethodsOrder),
  runTest: flowRight(dispatch, runTest),
  saveObjectiveParams: flowRight(dispatch, saveObjectiveParams)
});

export default connect(stateToProps, actionsToProps)(ObjectiveManage);
