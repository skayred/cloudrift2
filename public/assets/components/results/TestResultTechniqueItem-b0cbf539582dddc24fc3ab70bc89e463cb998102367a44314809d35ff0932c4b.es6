import ReactDOM from "react-dom";
import React, { DOM, PropTypes } from 'react';
import { Link } from 'react-router';

export default class TestResultTechniqueItem extends React.Component {
  constructor(props) {
    super(props);
  }

  getRowClassName = () => {
    if (this.props.result['status'] == 'finished') {
      return 'positive'
    } else if (this.props.result['status'] == 'failed') {
      return 'negative'
    }

    return ''
  }

  render() {
    return (
      DOM.tr({
          className: this.getRowClassName()
        },
        DOM.td({
          className: ''
        }, this.props.result['status']),
        DOM.td({
          className: ''
        }, this.props.result['reportMessage'])
      )
    );
  }
}
