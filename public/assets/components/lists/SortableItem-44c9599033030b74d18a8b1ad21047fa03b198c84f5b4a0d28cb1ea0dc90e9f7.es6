import React from 'react';
import { sortable } from 'react-anything-sortable';

@sortable
export default class SortableItem extends React.Component {
  render() {
    return (
      <div {...this.props}>
        your item #{this.props.sortData}
      </div>
    );
  }
};
