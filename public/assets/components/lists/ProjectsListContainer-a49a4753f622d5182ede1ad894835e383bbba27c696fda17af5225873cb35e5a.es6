import { connect } from 'react-redux';
import { createProject, updateProject } from '../../actions/createProjectAction.es6';
import { loadProjectsList, removeProject } from '../../actions/ProjectsListActions.es6';
import ProjectsList from './ProjectsList.es6';
import flowRight from 'lodash/function/flowRight';
import get from 'lodash/object/get';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createProject: flowRight(dispatch, createProject),
  updateProject: flowRight(dispatch, updateProject),
  updateList: flowRight(dispatch, loadProjectsList),
  removeProject: flowRight(dispatch, removeProject)
});

export default connect(stateToProps, actionsToProps)(ProjectsList);
