import ReactDOM from 'react-dom';
import React, { DOM, PropTypes } from 'react';
import _ from 'lodash';
import Rx from 'rx';
import $ from 'jquery';

export default class GenericList extends React.Component {
    constructor(props) {
      super(props);

      this.placeholder = document.createElement('tr')
      this.placeholder.className = 'placeholder'
    }

    renderCreateButton = () => {
      if (this.props.canCreate) {
        return (DOM.button({
            className: 'ui button primary',
            key: 'create_button',
            onClick: this.props.createChild,
            style: {marginBottom: '10px'}
          },
          DOM.i({className: 'icon edit'}),
          (this.props.createCaption || 'Create')
        ))
      }

      return null
    }

    renderColumns = () => {
      if (this.props.hasActions) {
        return [..._.map(this.props.columnNames, (colname, i) => {
          return DOM.th({key: `column_name_${i}`}, colname)
        }), DOM.th({key: 'column_actions', className: 'collapsing'}, 'Actions')]
      } else {
        return [..._.map(this.props.columnNames, (colname, i) => {
          return DOM.th({key: `column_name_${i}`}, colname)
        })]
      }
    }

    render() {
      return (
        DOM.div(null,
          this.renderCreateButton(),
          DOM.div(
            {
              className: 'ui container',
              key: 'list'
            },
            DOM.table({
                className: `ui basic vertical scroll table small compact ${this.props.className}`
              },
              DOM.thead(null,
                DOM.tr(null,
                  this.renderColumns()
                )
              ),
              DOM.tbody(null, this.props.children)
            )
          )
        )
      )
    }
}

GenericList.defaultProps = {
  canCreate: true,
  hasActions: true,
  className: 'striped'
}
