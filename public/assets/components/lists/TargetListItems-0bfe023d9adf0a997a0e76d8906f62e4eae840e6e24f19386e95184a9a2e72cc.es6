import ReactDOM from 'react-dom';
import React, { DOM, PropTypes } from 'react';
import { Link } from 'react-router';

export default class TargetListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      DOM.tr(
        null,
        DOM.td({
          className: ''
        }, this.props.objective['name']),
        DOM.td({
          className: ''
        }, this.props.objective['description']),
        DOM.td(null,
          DOM.div({className: 'ui small basic icon buttons'},
            React.createElement(Link, {
              to: `/targets/${this.props.id}`,
              className: 'ui basic tiny button',
              'data-tooltip': 'Manage the project'
            }, DOM.i({className: 'icon configure'})),
            DOM.a({
              className: 'ui basic tiny button',
              'data-tooltip': 'Edit project',
              onClick: this.props.onEditClicked,
              href: 'javascript:void(0)'
            }, DOM.i({className: 'icon pencil'})),
            DOM.a({
              className: 'ui basic tiny button',
              'data-tooltip': 'Remove project',
              onClick: this.props.removeElement,
              href: 'javascript:void(0)'
            }, DOM.i({className: 'icon remove'}))
          )
        )
      )
    );
  }
}
