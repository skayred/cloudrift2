import ReactDOM from "react-dom";
import {Component, r, e} from '../../util/init-react.es6';
import GenericList from './GenericList.es6';
import ProjectDetailsModal from '../modals/ProjectDetailsModal.es6';
import ListItem from './ProjectListItem.es6';
import _ from 'lodash';

export default class ProjectsList extends Component {
  constructor(props) {
    super(props);

    this.state = {newItemShown: false, selectedProjectID: null}
  }

  createChild = () => {
    this.setState({newItemShown: true});
  }

  createProject = (name, description) => {
    this.props.createProject({}, {name: name, description: description});
    this.setState({newItemShown: !this.state.newItemShown});
  }

  updateProject = (id, name, description) => {
    this.props.updateProject(id, {name: name, description: description});
    this.setState({newItemShown: !this.state.newItemShown});
  }

  toggleCreateModal = () => {
    this.setState({newItemShown: !this.state.newItemShown});
  }

  removeProject = (projectID) => {
    this.props.removeProject(projectID);
  }

  render() {
    return (
      r.div(null,
        e(ProjectDetailsModal, {
          visible: this.state.newItemShown,
          onClose: this.toggleCreateModal,
          createProject: this.createProject,
          updateProject: this.updateProject,
          projectID: this.state.selectedProjectID,
          projects: this.props.projectsList.entries
        }),

        e(GenericList,
          {
            createChild: () => { this.setState({selectedProjectID: null}, () => { this.createChild() }) },
            columnNames: ['Project name', 'Description'],
            createCaption: 'Create test project'
          },
          _.map(this.props.projectsList.entries, (item) => {
            return e(ListItem, {
              id: item['id'],
              project: item,
              onEditClicked: () => {
                this.setState({selectedProjectID: item['id']}, () => { this.createChild() })
              },
              key: `projects_list_${item['id']}`,
              removeElement: () => { this.removeProject(item['id']) }
            })
          })
        )
      )
    );
  }
}
