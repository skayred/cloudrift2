import React, { DOM, PropTypes } from 'react';

import bind from 'lodash/function/bind';
import map from 'lodash/collection/map';

import Modal from '../util/Modal.es6';
import Text from '../forms/Text.es6';
import TextArea from '../forms/TextArea.es6';
import Select from 'react-select';

export default class ObjectiveDetailsModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      objective: {name: '', description: ''}
    }
  }

  isNew = () => {
    return (this.props.objectiveID ? false : true)
  }

  currentObjective = () => {
    return (_.find(this.props.objectives, {id: this.props.objectiveID}) || {name: '', description: ''})
  }

  onNameChange = (value) => {
    this.state.objective['name'] = value
    this.forceUpdate()
  }

  descriptionValueLink = () => {
    return (
      {
        value: this.state.objective['description'] || '',
        requestChange: (value) => { this.state.objective['description'] = value; this.forceUpdate() }
      });
  }

  render() {
    return (
      React.createElement(Modal, {
          header: this.isNew() ? 'Create test objective' : 'Edit test objective',
          active: this.props.visible,
          scrollable: true,
          overflow: 'visible',
          closable: true,
          actions: 'yesno',
          denyTitle: 'Close',
          approveTitle: 'Create',
          onShow: () => { this.state.objective = this.currentObjective(); this.forceUpdate(); },
          onApprove: () => {
            if (this.isNew()) {
              this.props.createObjective(this.state.objective['name'], this.state.objective['description'])
            } else {
              this.props.updateObjective(this.state.objective['id'], this.state.objective['name'], this.state.objective['description'])
            }
          },
          onDeny: this.props.onClose
        },
        DOM.div({
            className: 'ui form'
          },
          React.createElement(Text, {
            label: 'Objective name',
            name: 'name',
            value: this.state.objective['name'],
            placeholder: 'Objective name',
            onChange: this.onNameChange,
            style: {width: '15rem'}
          }),
          React.createElement(TextArea, {
            label: 'Objective description',
            name: 'name',
            placeholder: 'Objective description',
            valueLink: this.descriptionValueLink()
          }),
          React.createElement(Select, {
            multi: true,
            simpleValue: false,
            disabled: false,
            value: this.state.value,
            placeholder: 'Choose application attributes',
            options: [
	      { label: 'Ruby', value: 'chocolate' },
	      { label: 'Ruby on Rails', value: 'vanilla' },
	      { label: 'Web service', value: 'strawberry' }
            ],
            onChange: (value) => { this.setState({value: value}) }})
        )
      )
    );
  }
};
