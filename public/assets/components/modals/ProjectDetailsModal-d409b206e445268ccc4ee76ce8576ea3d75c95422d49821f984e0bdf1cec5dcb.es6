import React, { DOM, PropTypes } from 'react';

import bind from 'lodash/function/bind';
import map from 'lodash/collection/map';

import Modal from '../util/Modal.es6';
import Text from '../forms/Text.es6';
import TextArea from '../forms/TextArea.es6';

export default class ProjectDetailsModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      project: {name: '', description: ''}
    }
  }

  isNew = () => {
    return (this.props.projectID ? false : true)
  }

  currentProject = () => {
    return (_.find(this.props.projects, {id: this.props.projectID}) || {name: '', description: ''})
  }

  onNameChange = (value) => {
    this.state.project['name'] = value
    this.forceUpdate()
  }

  descriptionValueLink = () => {
    return (
      {
        value: this.state.project['description'] || '',
        requestChange: (value) => { this.state.project['description'] = value; this.forceUpdate() }
      });
  }

  render() {
    return (
      React.createElement(Modal, {
          header: this.isNew() ? 'Create test project' : 'Edit test project',
          active: this.props.visible,
          scrollable: true,
          closable: true,
          actions: 'yesno',
          denyTitle: 'Close',
          approveTitle: 'Create',
          onShow: () => { this.state.project = this.currentProject(); this.forceUpdate(); },
          onApprove: () => {
            if (this.isNew()) {
              this.props.createProject(this.state.project['name'], this.state.project['description'])
            } else {
              this.props.updateProject(this.state.project['id'], this.state.project['name'], this.state.project['description'])
            }
          },
          onDeny: this.props.onClose
        },
        DOM.div({
          className: 'ui form'
        },
          React.createElement(Text, {
            label: 'Project name',
            name: 'name',
            placeholder: 'Project name',
            value: this.state.project['name'],
            onChange: this.onNameChange
          }),
          React.createElement(TextArea, {
            label: 'Project description',
            name: 'name',
            placeholder: 'Project description',
            valueLink: this.descriptionValueLink()
          }),
        )
      )
    );
  }
};

