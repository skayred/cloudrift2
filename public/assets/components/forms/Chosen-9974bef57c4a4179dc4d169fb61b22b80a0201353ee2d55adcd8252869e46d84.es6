import React, { DOM, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import $ from 'jquery';
import chosenPlugin from 'vanilla-chosen';
import _ from 'lodash';

import SemanticField from './SemanticField.es6';

export default class Text extends React.Component {
  getValueLink = (props) => {
    return ({
      value: props.value,
      requestChange: (value) => { props.onChange(value) }
    });
  }

  //componentWillReceiveProps = (props) => {
  //  this.chosen.val(this.getValueLink(props).value).trigger('chosen:updated')
  //}

  componentDidMount = () => {
    this.chosen = $(ReactDOM.findDOMNode(this.refs.select))

    chosenPlugin(this.chosen)
  }

  render = () => {
    let options = _(this.props.options).map((option) => {
      return {
          key: option.value,
          value: option.value
      }
    })

    let props = _({}).extend(
      _(this.props).omit('label', 'placeholder', 'options'),
      {'ref': 'select', 'data-placeholder': this.props.placeholder, multiple: this.props.multiple}
    )

    return (React.createElement(SemanticField, {
          comment: this.props.comment,
          label: this.props.label,
          error: this.props.error
        },
        DOM.select({
          ref: 'select',
          'data-placeholder': this.props.placeholder,
          multiple: this.props.multiple
        }, null)
      )
    )
  }
}
