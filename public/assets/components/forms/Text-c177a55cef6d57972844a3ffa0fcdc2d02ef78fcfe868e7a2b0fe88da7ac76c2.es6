import React, { DOM, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import SemanticField from './SemanticField.es6';

export default class Text extends React.Component {
  getValueLink = () => {
    if (this.props.valueLink) {
      return this.props.valueLink
    } else {
      return {
        value: this.props.value,
        requestChange: (value) => this.props.onChange(value)
      }
    }
  }

  onMouseOver = (e) => {
    $(e.target).parents('li').attr('draggable', 'false')
  }

  onMouseOut = (e) => {
    $(e.target).parents('li').attr('draggable', 'true')
  }

  onBlur = (e) => {
    this.props.onBlur(e)
  }

  onKeyDown = (e) => {
    this.props.onKeyDown(e)
  }

  onClick = () => {
    if (this.props.autoSelect) {
      ReactDOM.findDOMNode(this.refs.input).select()
    }
  }

  render = () => {
    return (
      React.createElement(SemanticField, {
          comment: this.props.comment,
          label: this.props.label,
          error: this.props.error,
          className: this.props.className,
          disabled: this.props.disabled
        },
        DOM.input({
          type: this.props.type,
          name: this.props.name,
          ref: 'input',
          placeholder: this.props.placeholder,
          defaultValue: this.props.defaultValue,
          onMouseOver: this.onMouseOver,
          onMouseOut: this.onMouseOut,
          onClick: this.onClick,
          disabled: this.props.disabled,
          valueLink: this.getValueLink(),
        })
      ));
  }
}
