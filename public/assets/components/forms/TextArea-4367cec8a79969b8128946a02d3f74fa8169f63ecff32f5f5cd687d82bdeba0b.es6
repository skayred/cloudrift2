import React, { DOM, PropTypes } from 'react';

import SemanticField from './SemanticField.es6';

export default class TextArea extends React.Component {
  render = () => {
    return (
      React.createElement(SemanticField, {
          comment: this.props.comment,
          label: this.props.label,
          error: this.props.error
        },
        DOM.textarea({
          name: this.props.name,
          placeholder: this.props.placeholder,
          defaultValue: this.props.defaultValue,
          rows: this.props.rows,
          className: this.props.areaClassName,
          style: this.props.style,
          valueLink: this.props.valueLink
        })
      )
    )
  }
}
