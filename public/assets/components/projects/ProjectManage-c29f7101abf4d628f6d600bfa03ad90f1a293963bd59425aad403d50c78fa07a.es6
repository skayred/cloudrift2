import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'underscore'
import Rx from 'rx'

import ObjectiveDetailsModal from '../modals/ObjectiveDetailsModel.es6'
import GenericList from '../lists/GenericList.es6'
import TargetListItem from '../lists/TargetListItems.es6'
import Tester from '../tester/Tester.es6'
import Configurer from '../configurer/Configurer.js'

export default class ProjectManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {newItemShown: false, selectedObjectiveID: null}
  }

  createChild = () => {
    this.setState({newItemShown: true})
  }

  createObjective = (name, description) => {
    this.props.createObjective(this.props.params['slug'], name, description)
    this.setState({newItemShown: !this.state.newItemShown})
  }

  updateObjective = (objectiveID, name, description) => {
    this.props.updateObjective(this.props.params['slug'], objectiveID, name, description);
    this.setState({newItemShown: !this.state.newItemShown});
  }

  toggleCreateModal = () => {
    this.setState({newItemShown: !this.state.newItemShown});
  }

  removeObjective = (objectiveID) => {
    this.props.removeObjective(objectiveID);
  }

  getTargetsList = () => {
    let targets = []
    if (!!this.props.projectManage.project) {
      targets = this.props.projectManage.project.testObjectives
    }

    return targets
  }

  getAvailableParams = () => {
    let params = {}
    if (!!this.props.projectManage.project) {
      params = this.props.projectManage.project.availableParameters
    }

    return params
  }

  runTest = (params) => {
    this.props.runTest('project', this.props.params['slug'], params)
  }

  saveParams = (params) => {
    this.props.saveProjectParams(this.props.params['slug'], params)
  }

  render() {
    return (
      DOM.div(null,
        React.createElement(ObjectiveDetailsModal, {
          visible: this.state.newItemShown,
          onClose: this.toggleCreateModal,
          createObjective: this.createObjective,
          updateObjective: this.updateObjective,
          objectiveID: this.state.selectedObjectiveID,
          objectives: this.getTargetsList()
        }),

        React.createElement(
          Tester, {
            availableParams: this.getAvailableParams(),
            runTest: this.runTest,
            testResult: this.props.tests.testResult,
            history: this.props.history
          }
        ),
        React.createElement(
          Configurer, {
            availableParams: this.getAvailableParams(),
            saveParams: this.saveParams
          }
        ),

        React.createElement(GenericList,
          {
            createChild: this.createChild,
            columnNames: ['Test objective name', 'Objective description'],
            createCaption: 'Create test objective'
          },
          _.map(this.getTargetsList(), (item) => {
            return React.createElement(TargetListItem, {
              id: item['id'],
              key: `objective_list_${item['id']}`,
              objective: item,
              onEditClicked: () => {
                this.setState({selectedObjectiveID: item['id']}, () => { this.createChild() })
              },
              removeElement: () => { this.removeObjective(item['id']) }
            })
          })
        )
      )
    );
  }
}
