import { connect } from 'react-redux';
import { createTestObjective, updateTestObjective, removeTestObjective } from '../../actions/TestingTargetsAction.es6';
import { loadProjectsList, removeProject } from '../../actions/ProjectsListActions.es6';
import { runTest, saveProjectParams } from '../../actions/TestAction.es6';
import ProjectManage from './ProjectManage.es6';
import flowRight from 'lodash/function/flowRight';
import get from 'lodash/object/get';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createObjective: flowRight(dispatch, createTestObjective),
  updateObjective: flowRight(dispatch, updateTestObjective),
  removeObjective: flowRight(dispatch, removeTestObjective),
  runTest: flowRight(dispatch, runTest),
  saveProjectParams: flowRight(dispatch, saveProjectParams)
});

export default connect(stateToProps, actionsToProps)(ProjectManage);
