import React, { DOM, PropTypes } from 'react';
import ReactDOM from 'react-dom';

import bind from 'lodash/function/bind';
import map from 'lodash/collection/map';

import Dimmer from '../util/Dimmer.es6';
import { ESC , BACKSPACE } from '../../constants/KeyCodes.es6';

export default class Modal extends React.Component {
  componentDidMount = () => {
    this.bindEvents();
    this.handleDomEvent();
  }

  componentDidUpdate = (props) => {
    if (this.props.active != props.active) {
      this.handleDomEvent();
    }
  }

  withEl = (block) => {
    let el = ReactDOM.findDOMNode(this.refs.modal)
    if (el) {
      block($(el))
    }
  }

  bindEvents = () => {
    this.withEl((el) => {
      el.modal({
        detachable: false,
        closable: this.props.closable,
        observeChanges: true,
        onShow: () => { this.props.onShow ? this.props.onShow() : null },
        onApprove: () => { return false },
        onDeny: () => { return false }
      })
    })
  }

  handleDomEvent = () => {
    this.withEl((el) => {
      if (this.props.active)
        el.modal('show');
      else
        el.modal('hide');
    })
  }

  fireResult = (result) => {
    if (result) {
      return this.props.onApprove;
    } else {
      return this.props.onDeny;
    }
  }

  renderOk = () => {
    let title = this.props.approveTitle
    let yesClassName = this.props.approveClassName

    return (
      DOM.div({onClick: this.fireResult(true), className: `ui ${yesClassName} approve button`}, title)
    )
  }

  renderYesNo = () => {
    let yesTitle = this.props.approveTitle;
    let noTitle = this.props.denyTitle;
    let yesClassName = this.props.approveClassName;
    let noClassName = this.props.denyClassName;

    return ([
      DOM.div({onClick: this.fireResult(false), className: `ui ${noClassName} deny button`, key: 'no'}, noTitle),
      DOM.div({onClick: this.fireResult(true), className: `ui ${yesClassName} approve button`, key: 'yes'}, yesTitle)
    ])
  }

  renderCustom = () => {
    return this.props.actions;
  }

  renderActions = () => {
    let body = null

    if (this.props.actions == 'ok') {
      body = this.renderOk();
    } else if (this.props.actions == 'yesno') {
      body = this.renderYesNo();
    } else {
      body = this.renderCustom();
    }

    let clear = DOM.div({style: {clear: 'both'}, key: 'actionsClear'});
    let actions = DOM.div({className: 'actions', key: 'actionsBody'}, body);

    if (body) {
      return [clear, actions];
    } else {
      return null;
    }
  }

  render = () => {
    let size = this.props.size;
    let header = this.props.header;
    let children = this.props.children;
    let actions = this.props.actions;

    let contentStyle = {};
    if (this.props.scrollable) {
      contentStyle = { maxHeight: '70vh', overflow: this.props.overflow || 'auto' };
    }

    return (
      DOM.div({
        className: 'ui modal',
        style: this.props.style,
        ref: 'modal'},
        DOM.div({className: 'header'}, header),
        DOM.div({className: 'content', style: contentStyle},
          this.props.children),
          this.renderActions()
        )
    )
  }
}
