
import React, { DOM, PropTypes, Component } from 'react';
import { findDOMNode } from 'react-dom';

import bind from 'lodash/function/bind';
import contains from 'lodash';
import findWhere from 'lodash/collection/findWhere';
import map from 'lodash/collection/map';
import noop from 'lodash/utility/noop';
import without from 'lodash/array/without';

import classnames from 'classnames';
import ensureArray from '../../helpers/ensureArray.es6';

import Label from './Label.es6';
import DropdownMenu from './DropdownMenu.es6';


class Dropdown extends Component {
  constructor() {
    super();
    this.state = {visible: false};
    this._handleClick = bind(this._handleClick, this);
  }

  componentWillMount() {
    document.addEventListener('click', this._handleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this._handleClick, false);
  }

  _hide() {
    this.state.visible && this._toggle();
  }

  _toggle() {
    this.setState({visible: !this.state.visible});
  }

  _handleClick(e) {
    if (findDOMNode(this).contains(e.target))
      this._toggle();
    else
      this._hide();
  }

  render() {
    let { valueLink, options, defaultOption, className, placeholder } = this.props;

    placeholder = (!valueLink.value||!valueLink.value.length)?placeholder:''; //плейсхолдер отображается только если нет value

    className = classnames('ui dropdown selection', className, {
      'active visible': this.state.visible
    });

    const valueToFind = valueLink.value? valueLink.value : defaultOption.value;
    const activeOption = findWhere([ defaultOption, ...options ], {value: valueToFind });

    const onOptionClick = (e,value) => {
      valueLink.requestChange(value);
    };

    const menu = React.createElement(DropdownMenu, {
        options: defaultOption
          ? [ defaultOption, ...options ]
          : options,
        valueLink, onOptionClick,
        visible: this.state.visible
      });

    return (
      DOM.div({className: 'ui select'},
        DOM.div({className},
          React.createElement(Text, {placeholder}, (activeOption || {}).text),
          React.createElement(Icon),
          menu)
        )
    );
  }
}
Dropdown.propTypes = {
  valueLink: PropTypes.shape({
    value: PropTypes.any,
    requestChange: PropTypes.func.isRequired
  }).isRequired,
  options: PropTypes.any.isRequired,
  defaultOption: PropTypes.shape({
    id: PropTypes.any,
    text: PropTypes.string
  }),
  className: PropTypes.string,
  placeholder: PropTypes.string
};

export default Dropdown;


//--- Input elements
const Text = ({ children, placeholder }) =>
  DOM.div({className: classnames('text', {default: !children})},
    children || placeholder);

const Icon = () => DOM.i({className: 'dropdown icon'});

const Labels = ({valueLink, options}) =>
  map(valueLink.value, value => {
    value = value + '';
    const onClick = () =>
      valueLink.requestChange( without(valueLink.value, value) );

    const text = findWhere(options, {value}).text;

    return (
      React.createElement(Label, { key: value, element: 'a',
        className: 'ui label transition visible' }, text,
        DOM.i({onClick, className: 'delete icon'}))
    )
  });

//#######

const isPlusMinus = (str) => (
  (!(str && str.length)) ? false
  : (str[0]=='+'||str[0]=='-')
);
const correctValue = (value) => {
  return ( ((isPlusMinus(value)) || isNaN(parseInt(value)))
    ? value
    : parseInt( value) )
    || null;
}
