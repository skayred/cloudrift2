import findIndex from 'lodash/array/findIndex';
import get from 'lodash/object/get';
import pick from 'lodash/object/pick';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadProjectsList } from '../../actions/ProjectsListActions.es6';

import ProjectsList from '../../components/lists/ProjectsListContainer.es6';

const projectsListLoad =
  {
    path: '/',

    component: ProjectsList,

    prepareData: (store, query, params, location) => {

      if (initialLoad()) return undefined;

      const { slug } = params;

      return store
        .dispatch(loadProjectsList())
        .tap(data => console.log(store, data.title))
    }
  };

export default projectsListLoad;
