import ProjectManage from './ProjectManage.es6';

import { loadProjectInfo } from '../../actions/ProjectsListActions.es6';

export default {
  childRoutes: [ProjectManage]
};
