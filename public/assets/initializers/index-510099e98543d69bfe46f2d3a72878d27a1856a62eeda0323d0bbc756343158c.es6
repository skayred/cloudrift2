import React from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, match, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

import createHistory from 'history/lib/createBrowserHistory';
import identity from 'lodash/utility/identity';

import createStore from '../store/index.es6';
import createRoutes from '../routes/index.es6';

import prepareData from '../helpers/prepareData.es6';

import MainLayout from '../components/MainLayout.es6';
import ProjectsList from '../components/lists/ProjectsList.es6';

const history = createHistory();
const store = createStore(window.__INITIAL_STATE__);
const routes = createRoutes();

function onNavigation(store, state, withoutScroll) {
  prepareData(store, state).subscribe(
    identity, identity, () => withoutScroll || window.scrollTo(0,0)
  );
}

history.listen(function (location) {
  match({ history, location, routes}, (error, redirect, state) => {
    const withoutScroll = (location.state || {}).withoutScroll;

    if (!error && !redirect) {
      onNavigation(store, state);
    };
  });
});

//!NOTE: Точка входа в приложение
ReactDOM.render(
  React.createElement(Provider, {store},
    React.createElement(Router, {routes, history} )),
    /*React.createElement(Router, {history: browserHistory},
      React.createElement(Route, {path: '/projects_list', component: ProjectsList})
    )
  ),*/
  document.getElementById('content'), //!NOTE  html-элемент, в который будет рендериться контент
  () => delete window.__INITIAL_STATE__
);
