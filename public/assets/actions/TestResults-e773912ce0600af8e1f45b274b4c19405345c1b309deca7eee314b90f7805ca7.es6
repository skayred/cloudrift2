import assign from 'lodash/object/assign';

import * as types from '../constants/actionTypes/TestResultTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function loadTestResults (query = {}) {
  query = assign({}, {}, query);

  return {
    [API_CALL]: {
      endpoint: '/test_results',
      query: {},
      types: [
        types.LOAD_TEST_RESULTS_REQUEST,
        types.LOAD_TEST_RESULTS_SUCCESS,
        types.LOAD_TEST_RESULTS_FAILURE
      ]
    }
  };
}

export function loadTestResultDetails (id, query = {}) {
  query = assign({}, {}, query);

  return {
    [API_CALL]: {
      endpoint: `/test_results/${id}?version=detailed`,
      query: {},
      types: [
        types.LOAD_TEST_RESULT_DETAILS_REQUEST,
        types.LOAD_TEST_RESULT_DETAILS_SUCCESS,
        types.LOAD_TEST_RESULT_DETAILS_FAILURE
      ]
    }
  };
}


export function removeResult(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_results/${id}`,
      method: 'DELETE',
      query: {},
      types: [
        types.DELETE_TEST_RESULT_REQUEST,
        types.DELETE_TEST_RESULT_SUCCESS,
        types.DELETE_TEST_RESULT_FAILURE
      ]
    }
  };
}
