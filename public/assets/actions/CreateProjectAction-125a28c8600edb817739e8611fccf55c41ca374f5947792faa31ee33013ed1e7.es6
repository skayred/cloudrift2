import * as types from '../constants/actionTypes/ProductsActionTypes.es6';
import { API_CALL } from '../middleware/API.es6';
import assign from 'lodash/object/assign';

export function createProject (data, query = {}) {
  query = assign({
    name: 'string',
    description: 'string'
  }, query);

  return {
    [API_CALL]: {
      endpoint: '/test_projects',
      method: 'POST',
      query: query,
      types: [
        types.ADD_PROJECTS_REQUEST,
        types.ADD_PROJECTS_SUCCESS,
        types.ADD_PROJECTS_FAILURE
      ]
    }
  }
}

export function updateProject (id, query = {}) {
  query = assign({
    name: 'string',
    description: 'string'
  }, query);

  return {
    [API_CALL]: {
      endpoint: `/test_projects/${id}`,
      method: 'PUT',
      query: query,
      types: [
        types.UPDATE_PROJECTS_REQUEST,
        types.UPDATE_PROJECTS_SUCCESS,
        types.UPDATE_PROJECTS_FAILURE
      ]
    }
  }
}
