import assign from 'lodash/object/assign';

import * as types from '../constants/actionTypes/TargetsActionTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function createTestObjective(projectID, objectiveName, objectiveDescription) {
  let query = {
    name: objectiveName,
    description: objectiveDescription,
    test_project_id: projectID
  };

  return {
    [API_CALL]: {
      endpoint: `/test_objectives`,
      method: 'POST',
      query: query,
      types: [
        types.CREATE_OBJECTIVE_REQUEST,
        types.CREATE_OBJECTIVE_SUCCESS,
        types.CREATE_OBJECTIVE_FAILURE
      ]
    }
  }
}

export function updateTestObjective(projectID, objectiveID, objectiveName, objectiveDescription) {
  let query = {
    name: objectiveName,
    description: objectiveDescription,
    testing_project_id: projectID
  };

  return {
    [API_CALL]: {
      endpoint: `/test_objectives/${objectiveID}`,
      method: 'PUT',
      query: query,
      types: [
        types.UPDATE_OBJECTIVE_REQUEST,
        types.UPDATE_OBJECTIVE_SUCCESS,
        types.UPDATE_OBJECTIVE_FAILURE
      ]
    }
  }
}

export function removeTestObjective(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_objectives/${id}`,
      method: 'DELETE',
      query: {},
      types: [
        types.DELETE_OBJECTIVE_REQUEST,
        types.DELETE_OBJECTIVE_SUCCESS,
        types.DELETE_OBJECTIVE_FAILURE
      ]
    }
  };
}

export function loadTargetDetails(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_objectives/${id}`,
      query: {version: 'detailed'},
      types: [
        types.LOAD_OBJECTIVE_DETAILS_REQUEST,
        types.LOAD_OBJECTIVE_DETAILS_SUCCESS,
        types.LOAD_OBJECTIVE_DETAILS_FAILURE
      ]
    }
  };
}
