import _ from 'lodash';
import Rx from 'rx';
import superagent from 'superagent';

let projectModel = {
  route: '/api/testing_projects',

  index(cb) {
    superagent
    .get(this.route)
    .accept('json')
    .end((err, resp) => cb(err, resp && resp.body));
  },

  index$: Rx.Observable.fromNodeCallback(projectModel.index.bind(projectModel))
}

export default projectModel;
