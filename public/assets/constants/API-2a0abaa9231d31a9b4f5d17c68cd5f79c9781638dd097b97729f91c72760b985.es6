require('../../../../lib/node/globals.js');

export let HOST;
export let PORT;
export let ENDPOINT;

export const PRODUCTION_HOST = 'http://cloudrift.x01d.com';

if (__DEVELOPMENT__) {
  HOST = 'http://localhost';
  PORT = 5000;
  ENDPOINT = '/api';
} else {
  HOST = PRODUCTION_HOST;
  PORT = 80;
  ENDPOINT = '/api';
}

export const URL = HOST + ':' + PORT + ENDPOINT;

export const INTERNAL_SERVER_ERROR = 500;
export const NOT_FOUND = 404;
