import assign from 'lodash/object/assign';
import map from 'lodash/collection/map';

import { decamelize } from 'humps';

//version - это вариант изображения - thumb, original и т.п. Смотри API.
export const mediaURL = ( media, version ) => {
  return media.image[version].url;
};

export const mediasWithURLs = (medias, version = 'large') => {
  return (
    map(medias, media => assign({
      url: media.image[version].url,
      width: media.image[version].width,
      height: media.image[version].height
    }, media))
  );
};

export const hallMediaURLs = (medias, version = 'large', type = 'none') => {
  if (type === 'none')
  return (
    map(medias, media => assign({
      url: media['photo']['url'],
      width: media.image[version].width,
      height: media.image[version].height
    }, media))
  );
  else return (
    map(medias, media => assign({
      url: media['photo']['${type}']['url'],
      width: media.image[version].width,
      height: media.image[version].height
    }, media))
  );
};

//TODO: Переименовать в createServicePath
export const serviceRoute = (id) => '/' + decamelize(id, {separator: '-'});

export const mediaRoute = (id, path = ':id(/:section)') =>
  serviceRoute(id) + '/' + path;

export const hallRoute = (id, path = ':id(/:section)') =>
  serviceRoute(id) + '/' + path;

export const panoramaRoute = (id,path = ':id(/:section)/tour') => 
  serviceRoute(id)+`/`+ path;

export const entityRoute = (id, path = ':slug') =>
serviceRoute(id) + '/' + path;

export const menuMediaURL = (serviceId) =>
  `/images/menu/${serviceId}.jpg`;
