import { parse } from 'qs';

import compact from 'lodash/array/compact';
import filter from 'lodash/collection/filter';
import identity from 'lodash/utility/identity';
import map from 'lodash/collection/map';
import has from 'lodash/object/has';

import Rx from 'rx';

export default function (store, state) {
  const { location, params, routes } = state;
  const query = parse(location.search.substr(1));

  const prepareDataFns = compact(map(routes, route => route.prepareData));
  const values = map(prepareDataFns, prepareData => {
    prepareData(store, query, params, location);
  });

  const observables = filter(values);

  return Rx.Observable.from(observables).flatMap(identity);
};
