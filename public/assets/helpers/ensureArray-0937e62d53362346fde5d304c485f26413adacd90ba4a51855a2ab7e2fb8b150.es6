import isArray from 'lodash/lang/isArray';

export default (data) => isArray(data) ? data : [data];
