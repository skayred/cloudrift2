import assign from 'lodash/object/assign'
import _ from 'lodash'

import * as types from '../constants/actionTypes/ProductsActionTypes.es6'
import * as target_types from '../constants/actionTypes/TargetsActionTypes.es6'
import * as paramsTypes from '../constants/actionTypes/TestParamsTypes.es6'
import { loadProjectDetails } from '../actions/ProjectsListActions.es6'
import { replaceElement } from '../util/arrayHelpers.es6'

const initialState = {
  isFetching: false,
  isErrored: false,
  project: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_PROJECT_DETAILS_REQUEST:
      return assign({}, state, {isFetching: true})
    case types.LOAD_PROJECT_DETAILS_SUCCESS:
      return assign({}, state, {project: action.response, isFetching: false})
    case types.LOAD_PROJECT_DETAILS_FAILURE:
      return assign({}, initialState, {isErrored: true})
    case target_types.CREATE_OBJECTIVE_SUCCESS:
      let newEntries = state.project.testObjectives
      newEntries.push(action.response)

      return assign({}, state, {entries: newEntries})
    case target_types.DELETE_OBJECTIVE_SUCCESS:
      let id = action.response['id']
      newEntries = state.project.testObjectives

      _.remove(newEntries, (proj) => {
        return (proj['id'] == id);
      })

      return assign({}, state, {entries: newEntries})
    case types.UPDATE_OBJECTIVE_SUCCESS:
      id = action.response['id']
      newEntries = state.project.testObjectives
      replaceElement(newEntries, id, action.response)

      return assign({}, state, {entries: newEntries});

    case paramsTypes.SAVE_PROJECT_PARAMS_SUCCESS:
      return assign({}, state, {project: action.response, isFetching: false})

    default:
      return state;
  }
};
