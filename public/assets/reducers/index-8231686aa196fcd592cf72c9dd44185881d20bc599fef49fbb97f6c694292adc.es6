import { combineReducers } from 'redux'

import projectsList from './ProjectsList.es6'
import projectManage from './ProjectManage.es6'
import targetManage from './TargetManage.es6'
import methodsList from './TestingMethods.es6'
import tests from './Tests.es6'
import testResults from './TestResults.es6'
import testResultDetails from './TestResultDetails.es6'

export default combineReducers({
  projectsList
  , projectManage
  , targetManage
  , methodsList
  , tests
  , testResults
  , testResultDetails
});
