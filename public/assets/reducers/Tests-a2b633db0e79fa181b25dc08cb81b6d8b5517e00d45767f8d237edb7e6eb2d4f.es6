import assign from 'lodash/object/assign'
import _ from 'lodash'

import * as types from '../constants/actionTypes/TestRunActionTypes.es6'
import loadProjectsList from '../actions/ProjectsListActions.es6'

const initialState = {
  isFetching: false,
  isErrored: false,
  testResult: null
}

export default function(state = initialState, action) {
  switch (action.type) {
    case types.RUN_TEST_REQUEST:
      return assign({}, state, {isFetching: true});
    case types.RUN_TEST_SUCCESS:
      return assign({}, state, {testResult: action.response, isFetching: false});
    case types.RUN_TEST_FAILURE:
      return assign({}, initialState, {isErrored: true});
    default:
      return state;
  }
};
