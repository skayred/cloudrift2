import assign from 'lodash/object/assign';
import _ from 'lodash';

import * as types from '../constants/actionTypes/ProductsActionTypes.es6';
import loadProjectsList from '../actions/ProjectsListActions.es6';
import { replaceElement } from '../util/arrayHelpers.es6';

const initialState = {
  isFetching: false,
  isErrored: false,
  count: 0,
  entries: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_PROJECTS_REQUEST:
      return assign({}, state, {isFetching: true});
    case types.LOAD_PROJECTS_SUCCESS:
      return assign({}, state, {entries: action.response, isFetching: false});
    case types.LOAD_PROJECTS_FAILURE:
      return assign({}, initialState, {isErrored: true});
    case types.ADD_PROJECTS_SUCCESS:
      let newEntries = state.entries;
      newEntries.push(action.response);

      return assign({}, state, {entries: newEntries});
    case types.DELETE_PROJECT_SUCCESS:
      let id = action.response['id'];
      newEntries = state.entries

      _.remove(newEntries, (proj) => {
        return (proj['id'] == id);
      })

      return assign({}, state, {entries: newEntries});
    case types.UPDATE_PROJECTS_SUCCESS:
      id = action.response['id'];
      newEntries = state.entries
      replaceElement(newEntries, id, action.response)

      return assign({}, state, {entries: newEntries});
    default:
      return state;
  }
};
