import assign from 'lodash/object/assign'
import _ from 'lodash'

import * as target_types from '../constants/actionTypes/TargetsActionTypes.es6'
import * as target_method_types from '../constants/actionTypes/TargetMethodsActionTypes.es6'
import * as paramsTypes from '../constants/actionTypes/TestParamsTypes.es6'

import { loadProjectDetails } from '../actions/ProjectsListActions.es6'

const initialState = {
  isFetching: false,
  isErrored: false,
  objective: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case target_types.LOAD_OBJECTIVE_DETAILS_REQUEST:
      return assign({}, state, {isFetching: true})

    case target_types.LOAD_OBJECTIVE_DETAILS_SUCCESS:
      return assign({}, state, {objective: action.response, isFetching: false})

    case target_types.LOAD_OBJECTIVE_DETAILS_FAILURE:
      return assign({}, initialState, {isErrored: true})

    case target_method_types.CREATE_TARGET_METHOD_SUCCESS:
      let newEntries = state.objective.testCases
      newEntries.push(action.response);

      return assign({}, state, {entries: newEntries});

    case target_method_types.DELETE_TARGET_METHOD_SUCCESS:
      let id = action.response['id'];
      newEntries = state.objective.testCases;

      _.remove(newEntries, (proj) => {
        return (proj['id'] == id);
      })

      return assign({}, state, {entries: newEntries});

    case paramsTypes.SAVE_OBJECTIVE_PARAMS_SUCCESS:
      return assign({}, state, {objective: action.response, isFetching: false})

    default:
      return state;
  }
};
