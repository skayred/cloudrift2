import { createStore, applyMiddleware, compose } from 'redux'
import React, { DOM, PropTypes } from 'react'

import APIMiddleware from '../middleware/API.es6'

import reducers from '../reducers/index.es6'

let create

create = function (initialState) {
  let store = compose(
    applyMiddleware(APIMiddleware)
  )(createStore)(reducers, initialState);

  if (module.hot)
    module.hot.accept('../reducers/index.es6', () => (
      store.replaceReducer(require('../reducers/index.es6'))
    ));

  return store;
};
export default create;
