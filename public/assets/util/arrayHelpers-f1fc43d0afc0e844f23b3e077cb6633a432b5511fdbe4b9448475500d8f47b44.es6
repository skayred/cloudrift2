export function replaceElement(array, id, element) {
  var index = _.findIndex(array, {id: id})

  if(index != -1) {
    return array.splice(index, 1, element)
  } else {
    return array.push(element)
  }
}
