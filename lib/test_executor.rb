class TestExecutor
  ALLOWED_TYPES = %w(objective project)

  def initialize(result_id, model_type, id, params)
    throw NameError unless ALLOWED_TYPES.include? model_type

    test_result = TestResult.find(result_id)

    model_class = "Test#{model_type.capitalize}".constantize
    model = model_class.find_by_id(id)

    throw Exception unless model

    model.test_cases.all.each do |test_case|
      unless execute_method test_result, test_case.test_technique, params
        break
      end
    end

    test_result.update_attributes(status: 2) unless test_result.failed?
  end

  def execute_method(test_result, method, params)
    params = params.to_unsafe_h if params.is_a? ActionController::Parameters

    p params.to_json

    uri = URI.parse(method.url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.read_timeout = 9999999
    request = Net::HTTP::Post.new('/method')
    request.add_field('Content-Type', 'application/json')
    request.body = params.to_json
    response = http.request(request)

    tech_result = TestTechniqueResult.create(test_result_id: test_result.id)

    result = JSON.parse response.body

    if result['status'] == 'ok'
      tech_result.update_attributes(status: 2, report_message: result['message'], report: result['verbose_message'])

      return true
    else
      tech_result.update_attributes(status: 1, report_message: result['message'], report: result['verbose_message'])
      test_result.update_attributes(status: 1)

      return false
    end
  end
end
