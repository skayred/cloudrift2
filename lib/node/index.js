var path = require('path');
var cors = require('cors')

require('app-module-path').addPath(path.join(process.cwd(), '../../app/assets/javascripts'));

require('babel-core/register');

require('./globals');
//var config = require('./config');
var config = require('../../config/webpack/config.js');

var express = require('express');
var morgan = require('morgan');

const staticDir = 'src/static';
const publicDir = '../../public';
const assetsDir = publicDir + '/assets';
const assetsPublicPath = '/assets';
const manifestFile = 'webpack-manifest.json';
const configSemanticDir = 'node_modules/semantic-ui/dist';
var semanticDir = 'node_modules/semantic-ui/dist';
var jqueryDir = 'node_modules/jquery/dist';
var sortableDir = 'node_modules/react-anything-sortable';
var chosenDir = 'node_modules/vanilla-chosen';

var application = express();
application.set('views', __dirname);
application.set('view engine', 'ejs');
application.use(morgan('combined'));
application.use(express.static(staticDir));
application.use(cors());

if (!__DEVELOPMENT__)
  application.use(express.static(publicDir));

if (__DEVELOPMENT__) {
  var webpack = require('webpack');
  var webpackDev = require('webpack-dev-middleware');
  var webpackHot = require('webpack-hot-middleware');
  var webpackConfig = require('../../config/webpack/config.js');
  var compiler = webpack(webpackConfig);
  var semanticDir = path.join(process.cwd(), configSemanticDir);

  const middleware = webpackDev(compiler, {
      publicPath: config.output.publicPath,
      contentBase: '../../app/assets/javascripts',
      stats: {
          colors: true,
          hash: false,
          timings: true,
          chunks: false,
          chunkModules: false,
          modules: false
      }
  });

  application.use(middleware);
  application.use(webpackHot(compiler));
  application.use('/semantic_assets', express.static(semanticDir));
  application.use('/jquery_assets', express.static(jqueryDir));
  application.use('/react_assets', express.static(sortableDir));
  application.use('/chosen_assets', express.static(chosenDir));
}

//application.get('*', require('./render'));
application.listen(process.env.PORT || 3001);
