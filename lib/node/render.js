
import React from 'react';
import ReactDOMServer from 'react-dom/server';

import DocumentMeta from 'react-document-meta';

import { RoutingContext, match } from 'react-router';
import { Provider } from 'react-redux';

import get      from 'lodash/object/get';
import find     from 'lodash/collection/find';
import identity from 'lodash/utility/identity';

let createStore  = require('store');
let createRoutes = require('routes');

import prepareData from '../../app/assets/javascripts/helpers/prepareData.es6';

//!NOTE: Белое окно ошибки здесь
const handleError = (e, res) => {
  if (__DEVELOPMENT__)
    res.status(500).send(e.stack);
  else
    res.status(500).send(e.stack);
};

let reload;

if (__DEVELOPMENT__)
  reload = require('./reload')(require);

const renderCallback = (res, store, renderProps) => () => {
  try {

    const content = ReactDOMServer.renderToString(
      React.createElement(Provider, {store},
        React.createElement(RoutingContext, renderProps))
    );

    const storeState = store.getState();
    const initialState = JSON.stringify( storeState );

    const status = get(storeState,['pageState','0','status'],0)
      || (()=>lg.warn('WARN: pageState.status not found in initialState')||200)();
    res.status(status);

    res.render('index', {
      selection: storeState.selections[0] || {},
      meta: DocumentMeta.renderAsHTML(),
      content: content,
      webpackAsset: webpackAsset,
      initialState
    })

  } catch(e) {
    handleError(e, res);
  }
};


const onRoutesMatched = (req, res, store, error, redirectLocation, renderProps) => {
  try {

    lg.info("\nREQUEST ON "+req.url);

    if (redirectLocation)
      res.status(301).redirect(redirectLocation.pathname + redirectLocation.search);
    else if (error)
      res.status(500).send(error.message);
    else if (renderProps == null)
      res.status(404).send('Not found');
    else {

      const onComplete = renderCallback(res, store, renderProps);
      prepareData(store, renderProps).subscribe(
        identity, onComplete, onComplete
      );

    };
  } catch(e) {
    handleError(e, res);
  };
}


export default (req, res) => {
  try {
    if (__DEVELOPMENT__) {
      createStore = reload('store');
      createRoutes = reload('routes');
    }

    const store = createStore();
    const routes = createRoutes(); //selections()
    const selection_slug = req.url.split('/')[1];

    requestSelection(selection_slug, (selection) =>  {

      store.dispatch(setSelections([selection]));

      match({routes, location: req.url}, (error, redirectLocation, renderProps)=>
        onRoutesMatched( req, res, store, error, redirectLocation, renderProps )
      );

    });
  } catch(e) {
    handleError(e, res);
  };
};
