import path from 'path';
import webpack from 'webpack';

import config from '../../config/webpack/config.js';
import * as appConfig from './config';

export default {
  manifestFile: path.join(process.cwd(), appConfig.assetsDir, appConfig.manifestFile),

  entry: config.entry,

  output: {
    path: path.join(process.cwd(), appConfig.assetsDir),
    filename: '[name]-[chunkhash].js',
    chunkFilename: '[id]-[name]-[chunkhash].js',
    publicPath: appConfig.assetsPublicPath + '/'
  },

  plugins: [
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor-[chunkhash].js'),
    new webpack.optimize.UglifyJsPlugin({compress: {warnings: false}}),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.DefinePlugin({
      __CLIENT__: true,
      __SERVER__: false,
      __DEVTOOLS__: false,
      __DEVELOPMENT__: false
    })
  ],

  resolve: config.resolve,

  module: config.module
};
