/* eslint no-console: 0 */

var path = require('path');
var express = require('express');
var webpack = require('webpack');
var webpackMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');
var config = require('../../config/webpack/config.js');
var assetsPublicPath = '/assets';
var semanticDir = 'node_modules/semantic-ui/dist';
var jqueryDir = 'node_modules/jquery/dist';
var sortableDir = 'node_modules/react-anything-sortable';
var chosenDir = 'node_modules/vanilla-chosen';

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3001 : process.env.PORT;
const app = express();

if (isDeveloping) {
    const compiler = webpack(config);
    const middleware = webpackMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'app/assets/javascripts',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));
    app.use('/semantic_assets', express.static(semanticDir));
    app.use('/jquery_assets', express.static(jqueryDir));
    app.use('/react_assets', express.static(sortableDir));
    app.use('/chosen_assets', express.static(chosenDir));
    app.get('*', function response(req, res) {
        res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'public/index.html')));
        res.end();
    });
} else {
    app.use(express.static(__dirname + '/public'));
    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'public/index.html'));
    });
}

app.listen(port, '0.0.0.0', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==>  Listening on port %s', port, port);
});
