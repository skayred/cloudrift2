
require('babel-core/register');

var _ = require('lodash');
var fs = require('fs.extra');
var Rx = require('rx');
var path = require('path');
var crypto = require('crypto');
var webpack = require('webpack');

var config = require('./production');
var appConfig = require('./config');

function fileChecksum (file) {
  var observable = new Rx.Subject();
  var checksum = crypto.createHash('md5');
  var stream = fs.ReadStream(file);
  stream.on('data', function(data) { checksum.update(data); });
  stream.on('end', function() {
    observable.onNext(checksum.digest('hex'));
    observable.onCompleted();
  });
  return observable;
};

function copyFile (from, to) {
  var observable = new Rx.Subject();
  fs.copy(from, to, {replace: true}, function (error) {
    error ? observable.onError(error) :  observable.onCompleted();
  });
  return observable;
};

function copyDir (from, to) {
  fs.rmrfSync(to);
  var observable = new Rx.Subject();
  fs.copyRecursive(from, to, function (error) {
    error ? observable.onError(error) :  observable.onCompleted();
  });
  return observable;
};

function buildSemantic (cb) {
  var semanticCSS = path.join(process.cwd(), appConfig.semanticDir, 'semantic.min.css');
  var semanticThemes = path.join(process.cwd(), appConfig.semanticDir, 'themes');
  var productionThemes = path.join(process.cwd(), appConfig.assetsDir, 'themes');

  fileChecksum(semanticCSS).subscribe(function (checksum) {
    var fileName = 'semantic-' + checksum + '.css';
    var productionCSS = path.join(process.cwd(), appConfig.assetsDir, fileName);

    Rx.Observable.fromArray([
      copyFile(semanticCSS, productionCSS),
      copyDir(semanticThemes, productionThemes)
    ]).flatMap(_.identity)
      .subscribe(
        _.noop,
        function (error) { console.error(error); },
        function () { cb(productionCSS); }
      );
  });
};

function buildJS (dir, file, output, cb) {
  var jqueryJS = path.join(process.cwd(), dir, file);

  fileChecksum(jqueryJS).subscribe(function (checksum) {
    var fileName = output + checksum + '.js';
    var productionCSS = path.join(process.cwd(), appConfig.assetsDir, fileName);

    Rx.Observable.fromArray([
      copyFile(jqueryJS, productionCSS)
    ]).flatMap(_.identity)
      .subscribe(
        _.noop,
        function (error) { console.error(error); },
        function () { cb(productionCSS); }
      );
  });
};

webpack(config.default, function (_error, stats) {
  var manifest = stats.toJson().assetsByChunkName;
  buildSemantic(function (semanticPath) {
    manifest['semantic_assets/semantic.css'] = path.basename(semanticPath);
    fs.writeFile(config.default.manifestFile, JSON.stringify(manifest));
  });
  buildJS(appConfig.jqueryDir, 'jquery.min.js', 'jquery-', function (jqueryPath) {
    manifest['jquery_assets/jquery'] = path.basename(jqueryPath);
    fs.writeFile(config.default.manifestFile, JSON.stringify(manifest));
  });
  buildJS(appConfig.semanticDir, 'semantic.min.js', 'semantic-', function (jqueryPath) {
    manifest['semantic_assets/semantic'] = path.basename(jqueryPath);
    fs.writeFile(config.default.manifestFile, JSON.stringify(manifest));
  });
});
