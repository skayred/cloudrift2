export const staticDir = 'src/static';
export const publicDir = 'public';
export const assetsDir = publicDir + '/assets';
export const assetsPublicPath = '/assets';
export const manifestFile = 'webpack-manifest.json';
export const semanticDir = 'node_modules/semantic-ui/dist';
export const jqueryDir = 'node_modules/jquery/dist';
