class ParamsHelper
  def self.set_params(model_id, model_type, params)
    model_class = "Test#{model_type.capitalize}".constantize
    model = model_class.find_by_id(model_id)

    parameters = model.available_parameters

    params.each { |key, value|
      required = parameters[key]
      param = LaunchParameter.find_or_create_by(
        configurable: model,
        technique_option_id: required['id']
      )
      param.update_attributes(value: value)
    }

    model
  end
end
