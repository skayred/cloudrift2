namespace :deploy do
  namespace :assets do
    task :non_digested_versions do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'assets:link_non_digests'
          end
        end
      end
    end
  end
end
