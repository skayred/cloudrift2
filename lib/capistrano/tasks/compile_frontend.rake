namespace :deploy do
  namespace :assets do
    task :compile_frontend do
      on roles(:all) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute "cd #{release_path}; npm install; npm run build"
          end
        end
      end
    end
  end
end
