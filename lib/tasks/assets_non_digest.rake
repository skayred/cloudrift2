Rake::Task['assets:precompile'].enhance do
  Rake::Task['assets:link_non_digests'].invoke
end

namespace :assets do
  desc 'Create a non digest link on all assets.'
  task :link_non_digests => :environment do
    assets_root = File.join Rails.public_path, Rails.configuration.assets.prefix
    Sprockets::Manifest.new(assets_root).assets.each do |non_digested, with_digest|
      FileUtils.symlink(File.basename(with_digest), File.join(assets_root, non_digested), force: true)
    end
  end
end
