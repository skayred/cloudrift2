var path = require('path');
var webpack = require('webpack');
// var CarteBlanche = require('carte-blanche');
//var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    devtool: 'eval-source-map',
    entry: {
        bundle: [path.join(process.cwd(), 'app/assets/javascripts/initializers/index.es6')],
        vendor: [
          'classnames',
          'humps',
          'moment',
          'qs',
          'react',
          'react-document-meta',
          'react-daterange-picker',
          'react-dom',
          'react-redux',
          'react-router',
          'redux',
          'rx',
          'superagent',
          'redux-devtools'
        ]
    },
    output: {
        path: path.join(__dirname, '../../tmp'),
        filename: '[name].js',
        publicPath: '/'
    },
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.DefinePlugin({
          'process.env.NODE_ENV': JSON.stringify('development')
        })
        // new CarteBlanche({
          // componentRoot: './app/assets/javascripts/components'
        // })
    ],
    module: {
        preLoaders: [
            {
                test: /\.es6?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                  presets: ['es2015', 'react', 'stage-0'],
                  plugins: ["transform-decorators-legacy"]
                }
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                  presets: ['es2015', 'react', 'stage-0'],
                  plugins: ["transform-decorators-legacy"]
                }
            }
        ],
        loaders: [
            {
                test: /\.es6?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query:
                {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: ["transform-decorators-legacy"]
                }
            },
            {
                test: /\.js?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query:
                {
                    presets: ['es2015', 'react', 'stage-0'],
                    plugins: ["transform-decorators-legacy"]
                }
            },
            {
                test: /\.json?$/,
                loader: 'json'
            }, {
                test: /\.css$/,
                loader: 'style!css?modules&localIdentName=[name]---[local]---[hash:base64:5]'
            }]
    }
};
