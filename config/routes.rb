Rails.application.routes.draw do
  resources :main

  resource :sessions

  namespace :api do
    resources :test_techniques, only: [:create, :index, :update, :destroy]

    resources :test_projects, only: [:create, :index, :update, :destroy, :show]
    resources :test_objectives, only: [:create, :index, :update, :destroy, :show]
    resources :test_cases, only: [:create, :index, :update, :destroy] do
      collection do
        put 'reorder'
      end
    end
    resources :testing, only: [:create, :show]
    resources :test_params, only: [:create, :show]
    resources :test_results, only: [:index, :show, :destroy]
  end

  get '/' => 'main#index'
  get '*foo' => 'main#index', constraints: lambda { |request| !(
                                                      (request.path.starts_with? '/api') ||
                                                      (request.path.starts_with? '/assets') ||
                                                      (request.path.starts_with? '/stylesheet')
                                                    )}
end
