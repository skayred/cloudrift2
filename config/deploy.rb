# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'cloudrift'
set :repo_url, 'git@bitbucket.org:skayred/cloudrift2.git'

set :branch, fetch(:branch, 'master')
set :scm, :git
set :pty, true
set :keep_releases, 5
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

#after 'deploy:normalize_assets', 'deploy:assets:non_digested_versions'
#after 'deploy:normalize_assets', 'deploy:assets:compile_frontend'
