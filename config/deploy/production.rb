server "cloudrift.x01d.com", user: "rvm", roles: %w{web app db}, port: 2295

set :branch, :master
set :rails_env, 'production'
set :deploy_to, '/home/rvm/cloudrift'

set :rvm_ruby_version, '2.3.0'
