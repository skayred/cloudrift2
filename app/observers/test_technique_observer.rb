class TestTechniqueObserver < ActiveRecord::Observer
  observe :test_technique

  def after_save(tech)
    tech.logger.info('After save')
    trigger_techniques_reindex
  end

  def after_create(tech)
    tech.logger.info('After create')
    trigger_techniques_reindex
  end

  private

  def trigger_techniques_reindex
    PulseWorker.perform_async
  end
end
