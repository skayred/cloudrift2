class User < ActiveRecord::Base
  def self.current_user=(user)
     Thread.current[:current_user] = user
  end

  def self.current_user
     Thread.current[:current_user]
  end

  def password=(password)
    self.password_digest = BCrypt::Password.create(password)
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest) == password
  end

  def self.auth(auth)
    token = auth.gsub('Bearer ', '')

    begin
      JWT.decode(token, Rails.configuration.jwt[:secret], true, { :algorithm => 'HS256' })
    rescue
      nil
    end
  end

  def token
    JWT.encode({
      id: id,
      email: email,
      username: username
    }, Rails.configuration.jwt[:secret], 'HS256')
  end
end
