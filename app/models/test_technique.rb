# coding: utf-8
class TestTechnique < ActiveRecord::Base
  enum status: [:added, :active, :not_available]

  has_many :technique_options, dependent: :destroy
  has_many :test_cases, dependent: :destroy

  before_update :drop_status

  scope :not_connected, (
          lambda do
            where(status: [
                    TestTechnique.statuses[:added],
                    TestTechnique.statuses[:not_available]])
          end)

  private

  def drop_status
    self.status = 0 if url_changed?
  end
end
