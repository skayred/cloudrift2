class TechniqueOption < ActiveRecord::Base
  belongs_to :test_case
  serialize :possible_values, Array
end
