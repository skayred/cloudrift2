class TestResult < ActiveRecord::Base
  enum status: [:started, :failed, :finished]

  has_many :test_technique_results
end
