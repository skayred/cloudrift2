class TestTechniqueResult < ActiveRecord::Base
  enum status: [:started, :failed, :finished]

  belongs_to :test_result
end
