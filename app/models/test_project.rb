class TestProject < ActiveRecord::Base
  include Configurable

  default_scope { order(id: :asc) }

  has_many :launch_parameters, as: :configurable, dependent: :destroy

  has_many :test_objectives, dependent: :destroy
  has_many :test_cases, through: :test_objectives, dependent: :destroy
end
