class TestCase < ActiveRecord::Base
  belongs_to :test_objective
  belongs_to :test_technique

  has_many :launch_parameters, as: :configurable, dependent: :destroy

  scope :ordered, -> { order(ordinal: :asc, id: :asc) }
end
