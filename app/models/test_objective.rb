class TestObjective < ActiveRecord::Base
  include Configurable

  belongs_to :test_project
  has_many :test_cases, -> { ordered }, dependent: :destroy

  has_many :launch_parameters, as: :configurable, dependent: :destroy

  def reorder_methods(ids)
    last_index = 0

    ids.each_with_index do |method_id, i|
      method = test_cases.find_by_id(method_id)

      if method
        method.update_attributes(ordinal: last_index)
        last_index += 1
      end
    end
  end
end
