module Configurable
  extend ActiveSupport::Concern

  def available_parameters
    parameters = {}

    test_cases.each do |test_case|
      test_case.test_technique.technique_options.each do |opt|
        opt_value = opt.as_json

        launch_param = LaunchParameter.find_by(configurable: self, technique_option_id: opt.id)
        opt_value['value'] = launch_param.value if launch_param

        parameters[opt.key] = opt_value
      end
    end

    parameters
  end
end
