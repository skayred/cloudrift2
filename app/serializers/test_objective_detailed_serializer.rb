class TestObjectiveDetailedSerializer < ActiveModel::Serializer
  attributes :id, :name, :available_parameters

  has_many :test_cases, serializer: TestCaseSerializer
end
