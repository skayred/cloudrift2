class TestCaseSerializer < ActiveModel::Serializer
  attributes :id, :test_objective_id, :test_technique_id, :name, :ordinal

  def name
    if object.test_technique
      object.test_technique.name
    else
      nil
    end
  end
end
