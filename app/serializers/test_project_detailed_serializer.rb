class TestProjectDetailedSerializer < ActiveModel::Serializer
  attributes :id, :name, :available_parameters, :description

  has_many :test_objectives, serializer: TestObjectiveSerializer
end
