class TestObjectiveSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
