class TestProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
end
