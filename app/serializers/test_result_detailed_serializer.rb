class TestResultDetailedSerializer < ActiveModel::Serializer
  attributes :id, :status

  has_many :test_technique_results, serializer: TestTechniqueResultSerializer
end
