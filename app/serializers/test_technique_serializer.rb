class TestTechniqueSerializer < ActiveModel::Serializer
  attributes :id, :url, :status, :name
end
