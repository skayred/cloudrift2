class TestTechniqueResultSerializer < ActiveModel::Serializer
  attributes :id, :status, :report_message, :report
end
