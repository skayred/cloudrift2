# coding: utf-8

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :allow_iframe
  # before_action :restrict_access

  respond_to :json, :html

  protected

  def allow_iframe
    response.headers['x-frame-options'] = 'ALLOWALL'
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
    response.headers['Access-Control-Request-Method'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end

  def after_sign_in_path_for(resource)
    path = if request.env
             omniauth_params = request.env['omniauth.params']
             lpath = if omniauth_params
                       omniauth_params['referrer']
                     end
             unless lpath
               lpath = request.env['omniauth.origin']
             end
           end

    if path
      path
    else
      stored_location_for(resource) || root_path
    end
  end

  def hash_path
    {
        controller: controller_name,
        action: action_name,
        id: params[:id]
    }
  end

  def authenticate
    User.current_user = User.auth(request.headers[:Authorization])
  end

  def restrict_access
    empty_response 401 unless User.current_user
  end
  #
  # def set_current_user
  #   User.current_user = current_user
  # end

  def empty_response(status = 200)
    render status: status, nothing: true
  end

  def json_response(status, result, options = {})
    render({ status: status, json: result }.merge(options))
  end

  def resource_params
    params
  end

  def resource_class
    controller_name.classify.constantize
  end

  def set_locale
    I18n.locale = params[:locale]
  end

  def safe_resource_class
    @_safe_resource_class ||= controller_name.classify.safe_constantize
  end

  def resource_id
    params[:id]
  end

  def when_resource_exists
    resource = resource_class.find(resource_id)

    if resource
      yield resource
    else
      empty_response 404
    end
  end
end
