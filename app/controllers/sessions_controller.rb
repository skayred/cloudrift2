class SessionsController < ActionController::Base
  def create
    user = User.find_by_email(params[:login])

    if user && user.is_password?(params[:password])
      token = user.token
      json_response(200, {
        token: token
      });
    else
      json_response(401, {})
    end
  end

  private
  def json_response(status, json)
    render({ status: status, json: json })
  end
end
