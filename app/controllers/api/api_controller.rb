# coding: utf-8
module Api
  class ApiController < ApplicationController
    # before_filter :restrict_access
    after_filter :default_render
    skip_before_action :verify_authenticity_token

    wrap_parameters false

    def index(result = nil, options={})
      json_response(
          200,
          collection_postprocessing(result || resource_class),
          index_serializer.merge!(options)
      )
    end

    def create(&block)
      new_resource = resource_class.new resource_params
      if new_resource.save
        block && instance_exec(new_resource, &block)
        json_response 201, new_resource, serializer: serializer_class
      else
        json_response 400, errors: new_resource.errors
      end
    end

    def update(&block)
      when_resource_exists do |resource|
        if resource.update_attributes resource_params
          block && instance_exec(resource, &block)
          json_response 200, resource, serializer: serializer_class
        else
          json_response 400, errors: resource.errors
        end
      end
    end

    def show(&block)
      when_resource_exists do |resource|
        block && instance_exec(resource, &block)
        json_response 200, resource, serializer: serializer_class
      end
    end

    def destroy
      when_resource_exists do |resource|
        id = resource.id
        resource.destroy
        json_response 200, {status: 'success', id: id}
      end
    end

    protected

    def default_render
      empty_response unless response
    end

    def serializer_class
      "#{controller_name.singularize}_#{params[:version]}_serializer"
          .classify.constantize
    end

    def index_serializer
      if params[:page].present? && params[:per_page].present?
        { serializer: serializer_class }
      else
        { each_serializer: serializer_class }
      end
    end

    def collection_postprocessing(resource)
      query = resource

      if params[:page].present? && params[:per_page].present?
        paginated_resource_collection(query)
      else
        query.all
      end
    end

    def paginated_resource_collection(resource)
      [
          { totalRecords: resource.count },
          resource.paginate(page: params[:page], per_page: params[:per_page])
      ]
    end
  end
end
