# coding: utf-8

module Api
  class TestingController < ApiController
    protect_from_forgery except: :create

    def create
      test_result = TestResult.create(status: 0)

      TestExecutionWorker.perform_async(test_result.id, params[:test_set_type], params[:test_set_id], params[:test_params])

      json_response 200, { test_result_id: test_result.id }
    end

    def show
    end
  end
end
