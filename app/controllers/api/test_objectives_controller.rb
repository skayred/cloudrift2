# coding: utf-8

module Api
  class TestObjectivesController < ApiController
    protect_from_forgery except: :create

    def create
      project = TestProject.find_by_id(params[:test_project_id])

      if project
        new_target = TestObjective.new resource_params
        new_target.test_project = project
        if new_target.save
          json_response 201, new_target, serializer: serializer_class
        else
          json_response 400, errors: new_target.errors
        end
      else
        json_response 404, errors: 'Project not found'
      end
    end

    def resource_params
      params.permit(:test_method_id, :name, :description)
    end
  end
end
