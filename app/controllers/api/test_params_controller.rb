# coding: utf-8

require './lib/params_helper.rb'

module Api
  class TestParamsController < ApiController
    protect_from_forgery except: :create

    def create
      model = ParamsHelper.set_params(params[:test_set_id], params[:test_set_type], params[:test_params])

      json_response 200, model, serializer: "Test#{params[:test_set_type].capitalize}DetailedSerializer".constantize
    end

    def show
    end
  end
end
