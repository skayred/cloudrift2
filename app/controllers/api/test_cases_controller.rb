# coding: utf-8

module Api
  class TestCasesController < ApiController
    protect_from_forgery except: [:create, :reorder]

    def reorder
      target = TestObjective.find_by_id(params[:objective_id])

      target.reorder_methods(params[:ids]) if target
    end

    def resource_params
      params.permit(:test_objective_id, :test_technique_id)
    end
  end
end
