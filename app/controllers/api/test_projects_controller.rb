# coding: utf-8

module Api
  class TestProjectsController < ApiController
    protect_from_forgery except: :create

    def resource_params
      params.permit(:name, :description)
    end
  end
end
