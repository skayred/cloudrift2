# coding: utf-8

module Api
  class TestTechniquesController < ApiController
    protect_from_forgery except: [:create, :update, :destroy]

    def index
      if params[:type]
        if params[:type] == 'active'
          super TestTechnique.active
        else
          super
        end
      else
        super
      end
    end

    def resource_params
      params.permit(:name, :url)
    end
  end
end
