module ApplicationHelper
  def webpack_include_tag(script)
    src =
        if Rails.configuration.webpack[:use_manifest]
          manifest = Rails.configuration.webpack[:asset_manifest]
          filename = manifest[script]

          "#{compute_asset_host}/assets/#{filename}"
        else
          "http://localhost:3001/#{script}.js"
        end

    javascript_include_tag(src)
  end

  def webpack_include_style(script)
    src =
        if Rails.configuration.webpack[:use_manifest]
          manifest = Rails.configuration.webpack[:asset_manifest]
          filename = manifest[script]

          "#{compute_asset_host}/assets/#{filename}"
        else
          "http://localhost:3001/#{script}"
        end

    stylesheet_link_tag src
  end
end
