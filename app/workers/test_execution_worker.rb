class TestExecutionWorker
  include Sidekiq::Worker
  def perform(test_result_id, test_set_type, test_set_id, test_params)
    require './lib/test_executor.rb'

    TestExecutor.new(test_result_id, test_set_type, test_set_id, test_params)
  end
end
