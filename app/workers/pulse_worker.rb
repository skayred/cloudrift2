class PulseWorker
  include Sidekiq::Worker
  def perform
    methods = TestTechnique.not_connected

    methods.each do |method|
      method.update_column(:retry_count, method.retry_count + 1)
      method.update_column(:retried_at, DateTime.now)
      begin
        url = URI.parse(method.url + '/interface')
        req = Net::HTTP::Get.new(url.to_s)
        res = Net::HTTP.start(url.host, url.port) do |http|
          http.request(req)
        end

        process_method_info(method, res)
      rescue Exception => e
        Rails.logger.error "Failed to fetch info for #{method.id}"\
                           " on #{method.url}"
        Rails.logger.error e.message

        method.update_column(:status, TestTechnique::statuses[:not_available])
        method.update_column(:fail_reason, e.message)
      end
    end
  end

  private

  def process_method_info(method, rawResponse)
    response = JSON.parse rawResponse.body

    response['params'].each do |param|
      option = TechniqueOption.find_or_create_by(
        key: param['name'],
        parameter_type: param['type'],
        test_technique_id: method.id
      )
      option.update_column(:possible_values, param['values'])
    end

    method.update_column(:status, TestTechnique::statuses[:active])
    method.update_column(:fail_reason, nil)
  end
end
