import _ from 'lodash'

import * as types from '../constants/actionTypes/TestingMethodTypes.es6'
import { replaceElement } from '../util/arrayHelpers.es6'

const initialState = {
  isFetching: false,
  isErrored: false,
  count: 0,
  entries: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_METHODS_REQUEST:
      return Object.assign({}, state, {isFetching: true})

    case types.LOAD_METHODS_SUCCESS:
      return Object.assign({}, state, {entries: action.response, isFetching: false})

    case types.LOAD_METHODS_FAILURE:
      return Object.assign({}, initialState, {isErrored: true})

    case types.CREATE_TEST_TECHNIQUE_SUCCESS:
      let newEntries = state.entries
      newEntries.push(action.response)

      return Object.assign({}, state, {entries: newEntries})

    case types.DELETE_TEST_TECHNIQUE_SUCCESS:
      let id = action.response['id'];
      newEntries = state.entries

      _.remove(newEntries, (proj) => {
        return (proj['id'] == id)
      })

      return Object.assign({}, state, {entries: newEntries})

    case types.UPDATE_TEST_TECHNIQUE_SUCCESS:
      id = action.response['id']
      newEntries = state.entries
      replaceElement(newEntries, id, action.response)

      return Object.assign({}, state, {entries: newEntries})

    default:
      return state;
  }
};
