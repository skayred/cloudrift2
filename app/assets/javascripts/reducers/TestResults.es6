import _ from 'lodash';

import * as types from '../constants/actionTypes/TestResultTypes.es6';
import { replaceElement } from '../util/arrayHelpers.es6';

const initialState = {
  isFetching: false,
  isErrored: false,
  entries: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_TEST_RESULTS_REQUEST:
      return Object.assign({}, state, {isFetching: true})
    case types.LOAD_TEST_RESULTS_SUCCESS:
      return Object.assign({}, state, {entries: action.response, isFetching: false})
    case types.LOAD_TEST_RESULTS_FAILURE:
      return Object.assign({}, initialState, {isErrored: true})
    case types.DELETE_TEST_RESULT_SUCCESS:
      let id = action.response['id']
      let newEntries = state.entries

      _.remove(newEntries, (proj) => {
        return (proj['id'] == id);
      })

      return Object.assign({}, state, {entries: newEntries})

    default:
      return state;
  }
};
