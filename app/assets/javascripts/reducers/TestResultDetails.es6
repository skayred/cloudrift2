import _ from 'lodash';

import * as types from '../constants/actionTypes/TestResultTypes.es6';
import { replaceElement } from '../util/arrayHelpers.es6';

const initialState = {
  isFetching: false,
  isErrored: false,
  resultDetails: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_TEST_RESULT_DETAILS_REQUEST:
      return Object.assign({}, state, {isFetching: true})
    case types.LOAD_TEST_RESULT_DETAILS_SUCCESS:
      return Object.assign({}, state, {resultDetails: action.response, isFetching: false})
    case types.LOAD_TEST_RESULT_DETAILS_FAILURE:
      return Object.assign({}, initialState, {isErrored: true})
    default:
      return state;
  }
};
