import findIndex from 'lodash';
import get from 'lodash';
import pick from 'lodash';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadTargetDetails } from '../../actions/TestingTargetsAction.es6';
import { loadMethodsList } from '../../actions/TestingMethodActions.es6';

import ObjectiveManage from '../../components/targets/ObjectiveManageContainer.es6';

const targetManageLoad =
  {
    path: '/targets/:slug',

    component: ObjectiveManage,

    prepareData: (store, query, params, location) => {
      if (initialLoad()) return undefined;

      const { slug } = params;

      return store
        .dispatch(loadTargetDetails(slug))
        .tap(data => console.log(store, data.title))
    }
  };

export default targetManageLoad;
