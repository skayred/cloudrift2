import findIndex from 'lodash';
import get from 'lodash';
import pick from 'lodash';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadProjectDetails } from '../../actions/ProjectsListActions.es6';

import ProjectManage from '../../components/projects/ProjectManageContainer.es6';

const projectManageLoad =
  {
    path: '/projects/:slug',

    component: ProjectManage,

    prepareData: (store, query, params, location) => {
      if (initialLoad()) return undefined;

      const { slug } = params;

      return store
        .dispatch(loadProjectDetails(slug))
        .tap(data => console.log(store, data.title))
    }
  };

export default projectManageLoad;
