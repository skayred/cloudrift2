import findIndex from 'lodash';
import get from 'lodash';
import pick from 'lodash';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadTestResults } from '../../actions/TestResults.es6';

import ResultsList from '../../components/results/ResultsListContainer.es6';

const testResultsLoad =
  {
    path: '/results',

    component: ResultsList,

    prepareData: (store, query, params, location) => {
      if (initialLoad()) return undefined;

      const { slug } = params;

      return store
        .dispatch(loadTestResults())
        .tap(data => console.log(store, data.title))
    }
  };

export default testResultsLoad;
