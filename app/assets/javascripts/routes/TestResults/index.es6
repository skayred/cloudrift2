import TestResults from './TestResults.es6'

import { loadTestResults } from '../../actions/TestResults.es6'

export default {
  prepareData: store => {
    store.getState().projectsList.isFetching
      ? null
      : store.dispatch(loadTestResults())
  },

  childRoutes: [TestResults]
}
