import MainLayout from '../components/MainLayoutContainer.es6'
import ProjectsList from './ProjectsList/index.es6'
import ProjectManage from './ProjectManage/index.es6'
import TargetManage from './TargetManage/index.es6'
import TestResults from './TestResults/index.es6'
import TestResultDetails from './TestResultDetails/index.es6'
import TestTechniques from './TestTechniques'

export default function () {
  return [{
    component: MainLayout

    , childRoutes: [
      ProjectsList
      , ProjectManage
      , TargetManage
      , TestResults
      , TestResultDetails
      , TestTechniques
    ]
  }]
};
