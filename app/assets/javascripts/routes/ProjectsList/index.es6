import ProjectsList from './ProjectsList.es6';

import { loadProjectsList } from '../../actions/ProjectsListActions.es6';

export default {
  prepareData: store => {
    store.getState().projectsList.isFetching
      ? null
      : store.dispatch(loadProjectsList())
  },

  childRoutes: [ProjectsList]
};
