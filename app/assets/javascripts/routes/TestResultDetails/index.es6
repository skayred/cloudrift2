import TestResultDetails from './TestResultDetails.es6'

import { loadTestResultDetails } from '../../actions/TestResults.es6'

export default {
  prepareData: store => {
    store.getState().projectsList.isFetching
      ? null
      : store.dispatch(loadTestResultDetails())
  },

  childRoutes: [TestResultDetails]
}
