import findIndex from 'lodash';
import get from 'lodash';
import pick from 'lodash';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadTestResultDetails } from '../../actions/TestResults.es6';

import ResultDetails from '../../components/results/ResultDetailsContainer.es6';

const testResultDetailsLoad =
  {
    path: '/results/:slug',

    component: ResultDetails,

    prepareData: (store, query, params, location) => {
      if (initialLoad()) return undefined

      const { slug } = params

      return store
        .dispatch(loadTestResultDetails(slug))
        .tap(data => console.log(store, data.title))
    }
  };

export default testResultDetailsLoad;
