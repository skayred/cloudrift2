import findIndex from 'lodash';
import get from 'lodash';
import pick from 'lodash';
import initialLoad from '../../helpers/initialLoad.es6';
import { serviceRoute, entityRoute } from '../../helpers/routesHelper.es6';

import { loadMethodsList } from '../../actions/TestingMethodActions.es6';

import TestTechniqueList from '../../components/techniques/TestTechniquesListContainer.js';

const testTechniqueList =
  {
    path: '/techniques',

    component: TestTechniqueList,

    prepareData: (store, query, params, location) => {
      if (initialLoad()) return undefined;

      const { slug } = params;

      return store
        .dispatch(loadMethodsList())
        .tap(data => console.log(store, data.title))
    }
  };

export default testTechniqueList;
