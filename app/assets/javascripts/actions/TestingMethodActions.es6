import * as types from '../constants/actionTypes/TestingMethodTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function loadMethodsList(techniquesType = null) {
  let query = {}

  if (!!techniquesType) {
    query['type'] = techniquesType
  }

  return {
    [API_CALL]: {
      endpoint: '/test_techniques',
      query: query,
      types: [
        types.LOAD_METHODS_REQUEST,
        types.LOAD_METHODS_SUCCESS,
        types.LOAD_METHODS_FAILURE
      ]
    }
  };
}

export function createTestTechnique(techniqueName, techniqueURL) {
  let query = {
    name: techniqueName,
    url: techniqueURL
  }

  return {
    [API_CALL]: {
      endpoint: `/test_techniques`,
      method: 'POST',
      query: query,
      types: [
        types.CREATE_TEST_TECHNIQUE_REQUEST,
        types.CREATE_TEST_TECHNIQUE_SUCCESS,
        types.CREATE_TEST_TECHNIQUE_FAILURE
      ]
    }
  }
}

export function updateTestTechnique(techniqueID, techniqueName, techniqueURL) {
  let query = {
    name: techniqueName,
    url: techniqueURL
  }

  return {
    [API_CALL]: {
      endpoint: `/test_techniques/${techniqueID}`,
      method: 'PUT',
      query: query,
      types: [
        types.UPDATE_TEST_TECHNIQUE_REQUEST,
        types.UPDATE_TEST_TECHNIQUE_SUCCESS,
        types.UPDATE_TEST_TECHNIQUE_FAILURE
      ]
    }
  }
}

export function deleteTestTechnique(techniqueID) {
  return {
    [API_CALL]: {
      endpoint: `/test_techniques/${techniqueID}`,
      method: 'DELETE',
      types: [
        types.DELETE_TEST_TECHNIQUE_REQUEST,
        types.DELETE_TEST_TECHNIQUE_SUCCESS,
        types.DELETE_TEST_TECHNIQUE_FAILURE
      ]
    }
  }
}
