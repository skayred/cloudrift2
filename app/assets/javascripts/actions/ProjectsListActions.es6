import * as types from '../constants/actionTypes/ProductsActionTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function loadProjectsList (query = {}) {
  query = Object.assign({}, {}, query);

  return {
    [API_CALL]: {
      endpoint: '/test_projects',
      query: {},
      types: [
        types.LOAD_PROJECTS_REQUEST,
        types.LOAD_PROJECTS_SUCCESS,
        types.LOAD_PROJECTS_FAILURE
      ]
    }
  };
}

export function removeProject(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_projects/${id}`,
      method: 'DELETE',
      query: {},
      types: [
        types.DELETE_PROJECT_REQUEST,
        types.DELETE_PROJECT_SUCCESS,
        types.DELETE_PROJECT_FAILURE
      ]
    }
  };
}

export function loadProjectDetails(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_projects/${id}`,
      query: {version: 'detailed'},
      types: [
        types.LOAD_PROJECT_DETAILS_REQUEST,
        types.LOAD_PROJECT_DETAILS_SUCCESS,
        types.LOAD_PROJECT_DETAILS_FAILURE
      ]
    }
  };
}
