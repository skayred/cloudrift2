import * as types from '../constants/actionTypes/PageStateActionTypes.es6';

export const setPageState = (state = []) => ({
  type: types.SET_PAGE_STATE,
  data: state
});
