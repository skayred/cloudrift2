import * as types from '../constants/actionTypes/TargetMethodsActionTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function createTargetMethod (targetID, methodID) {
  let query = {
    test_objective_id: targetID,
    test_technique_id: methodID
  };

  return {
    [API_CALL]: {
      endpoint: `/test_cases`,
      method: 'POST',
      query: query,
      types: [
        types.CREATE_TARGET_METHOD_REQUEST,
        types.CREATE_TARGET_METHOD_SUCCESS,
        types.CREATE_TARGET_METHOD_FAILURE
      ]
    }
  }
}

export function removeTargetMethod(id) {
  return {
    [API_CALL]: {
      endpoint: `/test_cases/${id}`,
      method: 'DELETE',
      query: {},
      types: [
        types.DELETE_TARGET_METHOD_REQUEST,
        types.DELETE_TARGET_METHOD_SUCCESS,
        types.DELETE_TARGET_METHOD_FAILURE
      ]
    }
  };
}


export function saveMethodsOrder(objective_id, ids) {
  return {
    [API_CALL]: {
      endpoint: '/test_cases/reorder',
      method: 'PUT',
      query: {ids: ids, objective_id: objective_id},
      types: [
        types.SAVE_METHODS_ORDER_REQUEST,
        types.SAVE_METHODS_ORDER_SUCCESS,
        types.SAVE_METHODS_ORDER_FAILURE
      ]
    }
  };
}
