import * as types from '../constants/actionTypes/TestRunActionTypes.es6';
import * as paramsTypes from '../constants/actionTypes/TestParamsTypes.es6';
import { API_CALL } from '../middleware/API.es6';

export function runTest (testType, testID, params) {
  let query = {}
  query['test_set_type'] = testType
  query['test_set_id'] = testID
  query['test_params'] = params

  return {
    [API_CALL]: {
      endpoint: '/testing',
      method: 'POST',
      query: query,
      types: [
        types.RUN_TEST_REQUEST,
        types.RUN_TEST_SUCCESS,
        types.RUN_TEST_FAILURE
      ]
    }
  }
}

export function saveProjectParams (testID, params) {
  let query = {}
  query['test_set_type'] = 'project'
  query['test_set_id'] = testID
  query['test_params'] = params

  return {
    [API_CALL]: {
      endpoint: '/test_params?version=detailed',
      method: 'POST',
      query: query,
      types: [
        paramsTypes.SAVE_PROJECT_PARAMS_REQUEST,
        paramsTypes.SAVE_PROJECT_PARAMS_SUCCESS,
        paramsTypes.SAVE_PROJECT_PARAMS_FAILURE
      ]
    }
  }
}

export function saveObjectiveParams (testID, params) {
  let query = {}
  query['test_set_type'] = 'objective'
  query['test_set_id'] = testID
  query['test_params'] = params

  return {
    [API_CALL]: {
      endpoint: '/test_params?version=detailed',
      method: 'POST',
      query: query,
      types: [
        paramsTypes.SAVE_OBJECTIVE_PARAMS_REQUEST,
        paramsTypes.SAVE_OBJECTIVE_PARAMS_SUCCESS,
        paramsTypes.SAVE_OBJECTIVE_PARAMS_FAILURE
      ]
    }
  }
}

export function saveCaseParams (testID, params) {
  let query = {}
  query['test_set_type'] = 'case'
  query['test_set_id'] = testID
  query['test_params'] = params

  return {
    [API_CALL]: {
      endpoint: '/test_params?version=detailed',
      method: 'POST',
      query: query,
      types: [
        paramsTypes.SAVE_CASE_PARAMS_REQUEST,
        paramsTypes.SAVE_CASE_PARAMS_SUCCESS,
        paramsTypes.SAVE_CASE_PARAMS_FAILURE
      ]
    }
  }
}
