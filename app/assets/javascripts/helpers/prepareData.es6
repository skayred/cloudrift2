import { parse } from 'qs';

import compact from 'lodash';
import filter from 'lodash';
import identity from 'lodash';
import has from 'lodash';

import Rx from 'rx';

export default function (store, state) {
  const { location, params, routes } = state;
  const query = parse(location.search.substr(1));

  const prepareDataFns = routes.map(route => route.prepareData).filter(x => !!x);
  const values = prepareDataFns.map(prepareData => {
    prepareData(store, query, params, location);
  });

  const observables = filter(values);

  return Rx.Observable.from(observables).flatMap(identity);
};
