import isArray from 'lodash';

export default (data) => isArray(data) ? data : [data];
