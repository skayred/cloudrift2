
import Rx from 'rx';
import request from 'superagent';

import { stringify } from 'qs';
import { camelizeKeys, decamelizeKeys } from 'humps';

import initialLoad from '../helpers/initialLoad.es6';

import { URL as API_ROOT } from '../constants/API.es6';

function APICall (endpoint, method = 'GET', query = {}) {
  let subject = new Rx.Subject();

  request[method.toLowerCase()](API_ROOT + endpoint)
    .query(stringify(decamelizeKeys(query), {arrayFormat: 'brackets'}))
    .end(function (error, data) {
      if (error)
        subject.onError({error: error, response: camelizeKeys((data || {}).body)});
      else {
        subject.onNext(camelizeKeys((data || {}).body));
        subject.onCompleted();
      }
    });

  return subject;
}

export const API_CALL = 'API_CALL';

const nextAction = (action, data) => (
  Object.assign({}, action, data, {[API_CALL]: undefined})
);

export default store => next => action => {
  if (!action[API_CALL]) return next(action);
  if (initialLoad() && action[API_CALL]) {
    return undefined;
  }

  const { endpoint, method, query, types } = action[API_CALL];
  const [requestType, successType, failureType] = types;
  const signature = (new Date()).getTime();

  next(nextAction(action, {signature, type: requestType}));

  const observable = APICall(endpoint, method, query);

  const onError = error => (
    next(nextAction(action, {signature, error, type: failureType}))
  );

  const onSuccess = response => (
    next(nextAction(action, {signature, response, type: successType}))
  );

  observable.subscribe(onSuccess, onError);

  return observable;
};
