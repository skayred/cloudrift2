import React, { DOM, PropTypes } from 'react'
import { Link } from 'react-router'

export default class MainLayout extends React.Component {
  render() {
    return (
      DOM.div(null,
        DOM.div({className: 'ui top shadow fixed menu'},
          DOM.div({
            className: 'ui top inverted small menu',
            id: 'main-nav'
          },
            DOM.div({
              className: 'ui container'
            })
          ),
          DOM.div({
            className: 'ui container'
          },
            DOM.div({
              className: 'ui grid verticalPadded container'
            },
              DOM.div({className: 'three wide column'},
                React.createElement(Link, {to: '/'},
                  DOM.img({
                    src: '/assets/logo.png',
                    height: '40px',
                    style: { width: 'initial' }
                  })
                )
              ),
              DOM.div({className: 'thirteen wide column', id: 'mainmenu-parent'},
                DOM.ul({className: 'ui secondary pointing small stackable menu'},
                  DOM.li(null, React.createElement(Link, {to: '/', className: 'item'}, 'Projects')),
                  DOM.li(null, React.createElement(Link, {to: '/results', className: 'item'}, 'Results')),
                  DOM.li(null, React.createElement(Link, {to: '/techniques', className: 'item'}, 'Techniques')),
                  DOM.li(null, DOM.a({className: 'item'}, 'About'))
                )
              )
            )
          )
        ),
        DOM.div({
          id: 'boxed clearfix'
        },
          DOM.div({
            className: 'ui main container',
            style:{'marginTop':'15px','marginBottom':'15px'}
          },
          this.props.children)
        )
      )
    );
  }
};
