import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'underscore'
import Rx from 'rx'

import TestCaseDetailsModal from '../modals/TestCaseDetailsModal.es6'
import SortableList from '../lists/SortableList.es6'
import TestCaseListItem from '../lists/TestCaseListItem.es6'
import SortableItem from '../lists/SortableItem.es6'
import Tester from '../tester/Tester.es6'
import Configurer from '../configurer/Configurer.js'

export default class ObjectiveManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newItemShown: false,
      sortableKey: 0
    }
  }

  componentWillMount = () => {
    this.props.loadMethodsList('active')
  }

  componentWillUpdate = () => {
    this.state.sortableKey++
  }

  createChild = () => {
    this.setState({newItemShown: true});
  }

  createTargetMethod = (methodID) => {
    this.props.createTargetMethod(this.props.params['slug'], methodID);
    this.setState({newItemShown: !this.state.newItemShown});
  }

  toggleCreateModal = () => {
    this.setState({newItemShown: !this.state.newItemShown});
  }

  getDraggableItems = () => {
    let boundMethods = []
    if (!!this.props.targetManage.objective) {
      boundMethods = this.props.targetManage.objective.testCases
    }

    return _.map(boundMethods, (item, index) => {
      return (React.createElement(
        TestCaseListItem, {
          sortData: item['id'],
          id: item['id'],
          key: `target_methods_item_${item['id']}`,
          removeElement: () => { this.props.removeTargetMethod(item['id']) }
        }, DOM.div(null, `${item['id']} ${item['name']}`)))
    })
  }

  handleSort = (newOrder) => {
    this.props.saveMethodsOrder(this.props.params['slug'], newOrder)
  }

  getAvailableParams = () => {
    let params = {}
    if (!!this.props.targetManage.objective) {
      params = this.props.targetManage.objective.availableParameters
    }

    return params
  }

  runTest = (params) => {
    this.props.runTest('objective', this.props.params['slug'], params)
  }

  saveParams = (params) => {
    this.props.saveObjectiveParams(this.props.params['slug'], params)
  }

  render() {
    return (
      DOM.div(null,
        React.createElement(
          TestCaseDetailsModal, {
            visible: this.state.newItemShown,
            onClose: this.toggleCreateModal,
            createTargetMethod: this.createTargetMethod,
            methodsList: this.props.methodsList
          }
        ),

        React.createElement(
          Tester, {
            availableParams: this.getAvailableParams(),
            runTest: this.runTest
          }
        ),
        React.createElement(
          Configurer, {
            availableParams: this.getAvailableParams(),
            saveParams: this.saveParams
          }
        ),

        React.createElement(
          SortableList,{
            createChild: this.createChild,
            createCaption: 'Add test technique',
            handleSort: this.handleSort,
            sortableKey: this.state.sortableKey
          }, this.getDraggableItems()
        )
      )
    );
  }
}
