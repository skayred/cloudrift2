import { connect } from 'react-redux';
import { createTargetMethod, removeTargetMethod, saveMethodsOrder } from '../../actions/TargetMethodsAction.es6';
import { loadMethodsList } from '../../actions/TestingMethodActions.es6';
import { runTest, saveObjectiveParams } from '../../actions/TestAction.es6';
import ObjectiveManage from './ObjectiveManage.es6';
import flowRight from 'lodash';
import get from 'lodash';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createTargetMethod: (slug, id) => dispatch(createTargetMethod(slug, id)),
  removeTargetMethod: (id) => dispatch(removeTargetMethod(id)),
  loadMethodsList: (type) => dispatch(loadMethodsList(type)),
  saveMethodsOrder: (slug, id) => dispatch(saveMethodsOrder(slug, id)),
  runTest: (t,s,p) => dispatch(runTest(t,s,p)),
  saveObjectiveParams: (s,p) => dispatch(saveObjectiveParams(s,p))
});

export default connect(stateToProps, actionsToProps)(ObjectiveManage);
