import React, { DOM, PropTypes } from 'react';

import bind from 'lodash';
import map from 'lodash';

import Modal from '../util/Modal.es6';
import Text from '../forms/Text.es6';

export default class TestingConfigureModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      React.createElement(Modal, {
          header: 'Test parameters',
          active: this.props.visible,
          scrollable: true,
          closable: true,
          actions: 'yesno',
          denyTitle: 'Close',
          approveTitle: 'Run',
          onShow: this.props.onShow,
          onApprove: this.props.onApprove,
          onDeny: this.props.onClose
        },
        DOM.div({
          className: 'ui form'
        }, this.props.children)
      )
    );
  }
};

