import React, { DOM, PropTypes } from 'react';

import bind from 'lodash';
import _ from 'lodash';

import Modal from '../util/Modal.es6';
import Text from '../forms/Text.es6';
import Dropdown from '../util/Dropdown.es6';

export default class TestCaseDetailsModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {test_technique_id: -1}
  }

  getOptions = () => {
    return _.map(this.props.methodsList.entries, (item) => {
      return {
        value: item['id'],
        text: item['name']
      };
    });
  }

  methodValueLink = () => {
    return (
      {
        value: this.state.test_technique_id,
        requestChange: (value) => { console.log(value); this.setState({test_technique_id: value}) }
      });
  }

  render() {
    return (
      React.createElement(Modal, {
          header: 'Create test case',
          active: this.props.visible,
          scrollable: true,
          overflow: 'visible',
          closable: true,
          actions: 'yesno',
          denyTitle: 'Close',
          approveTitle: 'Create',
          onApprove: () => { this.props.createTargetMethod(this.state.test_technique_id) },
          onDeny: this.props.onClose
        },
        DOM.div({className: 'ui field'},
          React.createElement(Dropdown, {
            valueLink: this.methodValueLink(),
            options: this.getOptions(),
            className: '',
            placeholder: 'Choose testing method'
          })
        )
      )
    );
  }
};
