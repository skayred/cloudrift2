import React, { DOM, PropTypes } from 'react'

import bind from 'lodash'
import _ from 'lodash'

import Modal from '../util/Modal.es6'
import Text from '../forms/Text.es6'

export default class TestTechniqueDetailsModal extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      technique: {name: '', url: ''}
    }
  }

  isNew = () => {
    return (this.props.techniqueID ? false : true)
  }

  currentTech = () => {
    return (_.find(this.props.techniques, {id: this.props.techniqueID}) || {name: '', url: ''})
  }

  onFieldChange = (field, value) => {
    this.state.technique[field] = value
    this.forceUpdate()
  }

  render() {
    return (
      React.createElement(Modal, {
          header: this.isNew() ? 'Create test technique' : 'Edit test technique',
          active: this.props.visible,
          scrollable: true,
          closable: true,
          actions: 'yesno',
          denyTitle: 'Close',
          approveTitle: 'Create',
          onShow: () => { this.state.technique = this.currentTech(); this.forceUpdate(); },
          onApprove: () => {
            if (this.isNew()) {
              this.props.createTestTechnique(this.state.technique['name'], this.state.technique['url'])
            } else {
              this.props.updateTestTechnique(this.state.technique['id'], this.state.technique['name'], this.state.technique['url'])
            }
          },
          onDeny: this.props.onClose
        },
        DOM.div({
          className: 'ui form'
        },
          React.createElement(Text, {
            label: 'Technique name',
            name: 'name',
            placeholder: 'Technique name',
            value: this.state.technique['name'],
            onChange: (value) => { this.onFieldChange('name', value) }
          }),
          React.createElement(Text, {
            label: 'Technique URL',
            name: 'url',
            placeholder: 'Technique URL',
            value: this.state.technique['url'],
            onChange: (value) => { this.onFieldChange('url', value) }
          }),
        )
      )
    );
  }
};

