import React, { DOM, PropTypes } from 'react';
import { findDOMNode } from 'react-dom';

import bind from 'lodash';
import classnames from 'classnames';

export default class Dimmer extends React.Component {
  constructor() {
    super();
    this._handleClick = bind(this._handleClick, this);
  }

  _handleClick(e) {
    if (e.target == findDOMNode(this))
      this.props.onClick && this.props.onClick();
  }

  render() {
    const {className, onClick, children, style} = this.props;

    return DOM.div({
      onClick: this._handleClick,
      className: classnames('ui dimmer', className),
      style
    }, children);
  }
};

Dimmer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]),
  onClick: PropTypes.func
};
