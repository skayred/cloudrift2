import { DOM, PropTypes } from 'react';

import classnames from 'classnames';

const Label = ({ className, children, element, onClick }) => {
  className = classnames('ui label', className);
  return DOM[element || 'div']({className, onClick}, children);
};

Label.propTypes = {
  className: PropTypes.string,
  element: PropTypes.string,
  onClick: PropTypes.func,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default Label;
