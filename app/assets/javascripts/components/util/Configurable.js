import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'lodash'
import Rx from 'rx'
import Select from 'react-select'
import NotificationSystem from 'react-notification-system'

import TestingConfigureModal from '../modals/TestingConfigureModal.es6'
import Text from '../forms/Text.es6'
import Checkbox from '../forms/Checkbox.js'
import TextArea from '../forms/TextArea.es6'
import SemanticField from '../forms/SemanticField.es6'

export default class Configurable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      configureShown: false,
      params: {}
    }
  }

  fieldValueLink = (key) => {
    return (
      {
        value: this.state.params[key],
        requestChange: (value) => {
          this.state.params[key] = value
          this.forceUpdate()
        }
      });
  }

  renderEnum = (fieldParameters) => {
    return (
      React.createElement(SemanticField, {
          label: fieldParameters['key']
        },
        React.createElement(Select, {
          simpleValue: false,
          disabled: false,
          label: fieldParameters['key'],
          value: this.state.params[fieldParameters['key']],
          placeholder: 'Choose a value',
          options: _.map(fieldParameters['possibleValues'], (val) => {
            return { label: val, value: val }
          }),
          onChange: (value) => {
            this.state.params[fieldParameters['key']] = value['value']
            this.forceUpdate();
          }}
        )
      )
    )
  }

  renderBoolean = (fieldParameters) => {
    return (
      React.createElement(Checkbox, {})
    )
  }

  renderText = (fieldParameters) => {
    return(
      React.createElement(TextArea, {
        label: fieldParameters['key'],
        valueLink: this.fieldValueLink(fieldParameters['key'])
      })
    )
  }

  renderTextField = (fieldParameters) => {
    return (
      React.createElement(Text, {
        label: fieldParameters['key'],
        valueLink: this.fieldValueLink(fieldParameters['key'])
      })
    )
  }

  createField = (fieldParameters, key) => {
    switch (fieldParameters['parameterType']) {
      case 'enum':
        return React.cloneElement(this.renderEnum(fieldParameters), {key: key})
      case 'boolean':
        return React.cloneElement(this.renderBoolean(fieldParameters), {key: key})
      case 'hash':
        return React.cloneElement(this.renderText(fieldParameters), {key: key})
      case 'text':
        return React.cloneElement(this.renderText(fieldParameters), {key: key})
      default:
        return React.cloneElement(this.renderTextField(fieldParameters), {key: key})
    }
  }

  renderParametersForm = () => {
    let fields = []

    _.forOwn(this.props.availableParams, (value, key) => {
      fields.push(this.createField(value, key))
    })

    return fields
  }

  rereadLaunchParameters = () => {
    let self = this

    _.forOwn(this.props.availableParams, (value, key) => {
      let fieldParameters = value
      self.state.params[fieldParameters['key']] = fieldParameters['value']
    })

    this.forceUpdate()
  }

  toggleTestConfigModal = () => {
    this.setState({configureShown: !this.state.configureShown});
  }
}
