
import React, { DOM, PropTypes, Component } from 'react';

import classnames from 'classnames';
import ensureArray from '../../helpers/ensureArray.es6';

import map from 'lodash';
import contains from 'lodash';

const DropdownMenu = ({ options, valueLink, visible, multiple, onOptionClick }) => {
  const className = classnames('menu transition', {
    'animating slide down in visible': visible,
    'hiden': !visible
  }); //'menu transition animating slide down in visible'

  const menuItems = options.map(option =>
    React.createElement( MenuItem, {
      key: option.value,
      option,
      valueLink,
      multiple,
      onClick: (e) => { onOptionClick(e, option.value) }
    })
  );

  return DOM.div({className}, menuItems);
};

export default DropdownMenu;

//--
const MenuItem = ({ option, valueLink, multiple, onClick }) => {
  const value = option.value;
  const active = contains(ensureArray(valueLink.value || null), value);
  const className = classnames('item', {
    'active selected': active,
    'filtered': active && multiple
  });

  return DOM.div({ className, onClick }, option.text);
};
