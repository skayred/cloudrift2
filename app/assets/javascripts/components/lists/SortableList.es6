import ReactDOM from 'react-dom';
import React, { DOM, PropTypes } from 'react';
import Sortable from 'react-anything-sortable';

export default class SortableList extends React.Component {
    constructor(props) {
      super(props);
    }

    render() {
      return (
        DOM.div(null,
            DOM.button(
              {
                className: 'ui button primary',
                key: 'create_button',
                onClick: this.props.createChild,
                style: {marginBottom: '10px'}
              },
              DOM.i({className: 'icon edit'}),
              (this.props.createCaption || 'Create')
            ),
            React.createElement(Sortable, {
              onSort: this.props.handleSort,
              key: this.props.sortableKey,
              sortHandle: 'handle'
            }, this.props.children)
        )
      )
    }
}
