import ReactDOM from "react-dom";
import React, { DOM, PropTypes } from 'react';
import { Link } from 'react-router';
import { sortable } from 'react-anything-sortable';

@sortable
export default class TestCaseListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      DOM.div({
          ...this.props
        },
        DOM.div(null, DOM.i({className: 'icon sidebar handle', style: { float: 'left' }})),
        DOM.div(null,
          DOM.div({
              className: 'ui small basic icon buttons right floated',
              style: { marginTop: '-8px' }
            },
            React.createElement(Link, {
              to: `/projects/${this.props.id}`,
              className: 'ui basic tiny button',
              'data-tooltip': 'Manage the project'
            }, DOM.i({className: 'icon configure'})),
            DOM.a({
              className: 'ui basic tiny button',
              'data-tooltip': 'Edit project',
              onClick: this.props.onEditClicked,
              href: 'javascript:void(0)'
            }, DOM.i({className: 'icon pencil'})),
            DOM.a({
              className: 'ui basic tiny button',
              'data-tooltip': 'Remove project',
              onClick: this.props.removeElement,
              href: 'javascript:void(0)'
            }, DOM.i({className: 'icon remove'}))
          )
        ),
        DOM.div({
          style: { display: 'block' }
        }, this.props.children)
      )
    );
  }
}
