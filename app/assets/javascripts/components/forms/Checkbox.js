import React, { DOM, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class Text extends React.Component {
  render = () => {
    return (
      DOM.div({className: 'ui checkbox'},
        DOM.input({type: 'checkbox', className: 'hidden', tabIndex: 0}),
        DOM.label(null, this.props.label)
      )
    )
  }
}
