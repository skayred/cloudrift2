import React, { DOM, PropTypes } from 'react';
import ReactDOM from 'react-dom';

export default class SemanticField extends React.Component {
  render = () => {
    return (DOM.div({
        className: `ui field ${this.props.className}`
      },
      DOM.label(null, this.props.label),
      this.props.children
    ))
  }
}
