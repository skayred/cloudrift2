import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'lodash'
import Rx from 'rx'
import Select from 'react-select'
import NotificationSystem from 'react-notification-system'

import TestingConfigureModal from '../modals/TestingConfigureModal.es6'
import Text from '../forms/Text.es6'
import TextArea from '../forms/TextArea.es6'
import SemanticField from '../forms/SemanticField.es6'
import Configurable from '../util/Configurable.js'

export default class Configurer extends Configurable {
  constructor(props) {
    super(props);
    this.state = {
      configureShown: false,
      params: {}
    }
  }

  saveParams = () => {
    this.setState({configureShown: !this.state.configureShown})
    this.props.saveParams(this.state.params)
  }

  render = () => {
    return(
      DOM.div({
          style: { float: 'right' }
        },

        React.createElement(TestingConfigureModal, {
          visible: this.state.configureShown,
          onClose: this.toggleTestConfigModal,
          onShow: this.rereadLaunchParameters,
          onApprove: this.saveParams
        }, this.renderParametersForm()),

        DOM.button({
          className: 'ui button',
          onClick: this.toggleTestConfigModal
        },
          DOM.i({className: 'icon settings'}),
          'Parameters'
        )
      )
    )
  }
}
