import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'underscore'
import Rx from 'rx'

import GenericList from '../lists/GenericList.es6'
import TestResultListItem from './TestResultListItem.es6'

export default class ResultsList extends React.Component {
  removeResult = (id) => {
    this.props.removeResult(id)
  }

  render = () => {
    return (
      DOM.div(null,
        React.createElement(GenericList,
          {
            columnNames: ['Test result ID', 'Test result status'],
            createCaption: 'Create test objective',
            canCreate: false,
            className: ''
          },
          _.map(this.props.testResults.entries, (item) => {
            return React.createElement(TestResultListItem, {
              id: item['id'],
              key: `technique_results_list_${item['id']}`,
              result: item,
              onEditClicked: () => {
                this.setState({selectedObjectiveID: item['id']}, () => { this.createChild() })
              },
              removeElement: () => { this.removeResult(item['id']) }
            })
          })
        )
      )
    )
  }
}
