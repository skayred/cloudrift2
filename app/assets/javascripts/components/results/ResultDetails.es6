import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'underscore'
import Rx from 'rx'

import GenericList from '../lists/GenericList.es6'
import TestResultTechniqueListItem from './TestResultTechniqueItem.es6'

export default class ResultDetails extends React.Component {
  getTechResultsList = () => {
    let techResults = []
    if ((!!this.props.testResultDetails.resultDetails) && (!this.props.testResultDetails.isFetching)) {
      techResults = this.props.testResultDetails.resultDetails.testTechniqueResults
    }

    return techResults
  }

  render = () => {
    return (
      DOM.div(null,
        React.createElement(GenericList,
          {
            columnNames: ['Status', 'Short message'],
            createCaption: 'Create test objective',
            canCreate: false,
            hasActions: false,
            className: ''
          },
          _.map(this.getTechResultsList(), (item) => {
            return React.createElement(TestResultTechniqueListItem, {
              id: item['id'],
              key: `result_tech_details_list_${item['id']}`,
              result: item
            })
          })
        ),
        DOM.div({
          className: `ui ${this.props.testResultDetails.isFetching ? 'active' : ''} dimmer`
        }, DOM.div({className: 'ui loader'}))
      )
    )
  }
}
