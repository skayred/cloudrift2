import { connect } from 'react-redux'
import flowRight from 'lodash'
import get from 'lodash'

//import { createTestObjective, updateTestObjective, removeTestObjective } from '../../actions/TestingTargetsAction.es6'
import ResultDetails from './ResultDetails.es6'
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6'

const stateToProps = (state, props) => {
  return state
};

const actionsToProps = dispatch => ({
  //runTest: flowRight(dispatch, runTest)
});

export default connect(stateToProps, actionsToProps)(ResultDetails)
