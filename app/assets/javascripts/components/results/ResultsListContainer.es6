import { connect } from 'react-redux'
import flowRight from 'lodash'
import get from 'lodash'

import { removeResult } from '../../actions/TestResults.es6'
import ResultsList from './ResultsList.es6'
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6'

const stateToProps = (state, props) => {
  return state
};

const actionsToProps = dispatch => ({
  removeResult: (id) => dispatch(removeResult(id))
});

export default connect(stateToProps, actionsToProps)(ResultsList)
