import ReactDOM from "react-dom";
import React, { DOM, PropTypes } from 'react';
import { Link } from 'react-router';

export default class TestResultListItem extends React.Component {
  constructor(props) {
    super(props);
  }

  getRowClassName = () => {
    if (this.props.result['status'] == 'finished') {
      return 'positive'
    } else if (this.props.result['status'] == 'failed') {
      return 'negative'
    }

    return ''
  }

  render() {
    return (
      DOM.tr({
          className: this.getRowClassName()
        },
        DOM.td({
          className: ''
        }, this.props.result['id']),
        DOM.td({
          className: ''
        }, this.props.result['status']),
        DOM.td(null,
          DOM.div({className: 'ui small basic icon buttons'},
            React.createElement(Link, {
              to: `/results/${this.props.id}`,
              className: 'ui basic tiny button',
              'data-tooltip': 'Detailed results'
            }, DOM.i({className: 'icon unhide'})),
            DOM.a({
              className: 'ui basic tiny button',
              'data-tooltip': 'Remove result',
              onClick: this.props.removeElement,
              href: 'javascript:void(0)'
            }, DOM.i({className: 'icon remove'}))
          )
        )
      )
    );
  }
}
