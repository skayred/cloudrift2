import React from "react";
import ReactDOM from "react-dom";

class Auth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {test: 'foo'};
    }

    render() {
        return (
            React.DOM.div(null, this.state.test)
        );
    }
}

ReactDOM.render(React.createElement(Auth), document.getElementById('root'));
