import { connect } from 'react-redux';
import { createTestObjective, updateTestObjective, removeTestObjective } from '../../actions/TestingTargetsAction.es6';
import { loadProjectsList, removeProject } from '../../actions/ProjectsListActions.es6';
import { runTest, saveProjectParams } from '../../actions/TestAction.es6';
import ProjectManage from './ProjectManage.es6';
import flowRight from 'lodash';
import get from 'lodash';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createObjective: (name, description) => dispatch(createTestObjective(name, description)),
  updateObjective: (objectiveID, name, description) => dispatch(updateTestObjective(objectiveID, name, description)),
  removeObjective: (objectiveID) => dispatch(removeTestObjective(objectiveID)),
  runTest: (params) => dispatch(runTest(params)),
  saveProjectParams: (params) => dispatch(saveProjectParams(params))
});

export default connect(stateToProps, actionsToProps)(ProjectManage);
