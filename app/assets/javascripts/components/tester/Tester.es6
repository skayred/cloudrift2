import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'lodash'
import Rx from 'rx'
import Select from 'react-select'
import NotificationSystem from 'react-notification-system'

import TestingConfigureModal from '../modals/TestingConfigureModal.es6'
import Text from '../forms/Text.es6'
import TextArea from '../forms/TextArea.es6'
import SemanticField from '../forms/SemanticField.es6'
import Configurable from '../util/Configurable.js'

export default class Tester extends Configurable {
  constructor(props) {
    super(props);
    this.state = {
      configureShown: false,
      lastTestID: null,
      params: {}
    }
  }

  componentDidUpdate = () => {
    if ((!!this.props.testResult) && (this.state.lastTestID != this.props.testResult.testResultId)) {
      this.setState({lastTestID: this.props.testResult.testResultId}, () => {
        this.growlTestID()
      })
    }
  }

  growlTestID = () => {
    this.refs.growler.addNotification({
      message: `Test submitted with ID ${this.state.lastTestID}`,
      level: 'success',
      action: {
        label: 'See details',
        callback: () => {
          this.props.history.push(`/results/${this.state.lastTestID}`)
        }
      }
    });
  }

  runTest = () => {
    this.setState({configureShown: !this.state.configureShown})
    this.props.runTest(this.state.params)
  }

  render = () => {
    return(
      DOM.div({
          style: { float: 'right' }
        },

        React.createElement(NotificationSystem, {ref: 'growler'}),

        React.createElement(TestingConfigureModal, {
          visible: this.state.configureShown,
          onClose: this.toggleTestConfigModal,
          onApprove: this.runTest,
          onShow: this.rereadLaunchParameters,
        }, this.renderParametersForm()),

        DOM.button({
          className: 'ui button',
          onClick: this.toggleTestConfigModal
        },
          DOM.i({className: 'icon bug'}),
          'Run test'
        )
      )
    )
  }
}
