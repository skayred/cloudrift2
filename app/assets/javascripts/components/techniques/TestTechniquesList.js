import ReactDOM from 'react-dom'
import React, { DOM, PropTypes } from 'react'
import _ from 'lodash'

import GenericList from '../lists/GenericList.es6'
import TechniqueListItem from './TechniqueListItem.js'
import TestTechniqueDetailsModal from '../modals/TestTechniqueDetailsModal.js'

export default class TestTechniquesList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      newItemShown: false,
      techniqueID: null
    }
  }

  toggleCreateModal = () => {
    this.setState({newItemShown: !this.state.newItemShown})
  }

  createTestTechnique = (name, url) => {
    this.props.createTestTechnique(name, url);
    this.toggleCreateModal();
  }

  updateTestTechnique = (id, name, url) => {
    this.props.updateTestTechnique(id, name, url);
    this.toggleCreateModal();
  }

  render = () => {
    return (
      DOM.div({},
        React.createElement(
          TestTechniqueDetailsModal, {
            visible: this.state.newItemShown,
            onClose: this.toggleCreateModal,
            techniques: this.props.methodsList.entries,
            techniqueID: this.state.techniqueID,
            createTestTechnique: this.createTestTechnique,
            updateTestTechnique: this.updateTestTechnique
          }
        ),

        React.createElement(GenericList,
          {
            createChild: () => {
              this.setState({techniqueID: null}, () => {this.toggleCreateModal()})
            },
            columnNames: ['Test technique name', 'Test technique URL'],
            createCaption: 'Add test technique'
          },
          _.map(this.props.methodsList.entries, (item) => {
            return React.createElement(TechniqueListItem, {
              id: item['id'],
              key: `technique_list_${item['id']}`,
              technique: item,
              onEditClicked: () => {
                this.setState({techniqueID: item['id']}, () => { this.toggleCreateModal() })
              },
              removeElement: () => { this.props.deleteTestTechnique(item['id']) }
            })
          })
        )
      )
    )
  }
}
