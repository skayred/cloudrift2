import { connect } from 'react-redux';
import { createTestTechnique, updateTestTechnique, deleteTestTechnique } from '../../actions/TestingMethodActions.es6';
import TestTechniqueList from './TestTechniquesList.js';
import get from 'lodash';
import { INTERNAL_SERVER_ERROR, NOT_FOUND } from '../../constants/API.es6';

const stateToProps = (state, props) => {
  return state;
};

const actionsToProps = dispatch => ({
  createTestTechnique: (name, url) => dispatch(createTestTechnique(name, url)),
  updateTestTechnique: (id, name, url) => dispatch(updateTestTechnique(id, name, url)),
  deleteTestTechnique: (id) => dispatch(deleteTestTechnique(id))
});

export default connect(stateToProps, actionsToProps)(TestTechniqueList);
