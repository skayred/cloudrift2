class CreateTargetMethods < ActiveRecord::Migration
  def change
    create_table :target_methods do |t|
      t.integer :testing_target_id
      t.integer :testing_method_id

      t.timestamps null: false
    end
  end
end
