class TestingMethodsAddStatus < ActiveRecord::Migration
  def change
    add_column :testing_methods, :status, :integer, default: 0
  end
end
