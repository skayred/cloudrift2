class RenameFieldsInLaunchParameters < ActiveRecord::Migration
  def change
    rename_column :launch_parameters, :target_method_id, :test_case_id
    rename_column :launch_parameters, :method_option_id, :technique_option_id
  end
end
