class CreateMethodOptions < ActiveRecord::Migration
  def change
    create_table :method_options do |t|
      t.string :key
      t.string :value
      t.integer :target_method_id

      t.timestamps null: false
    end
  end
end
