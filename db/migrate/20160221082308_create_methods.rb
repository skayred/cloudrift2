class CreateMethods < ActiveRecord::Migration
  def change
    create_table :testing_methods do |t|
      t.string :name
      t.integer :endpoint

      t.timestamps null: false
    end
  end
end
