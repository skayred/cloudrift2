class CreateLaunchParameters < ActiveRecord::Migration
  def change
    create_table :launch_parameters do |t|
      t.integer :target_method_id
      t.integer :method_option_id
      t.string :value

      t.timestamps null: false
    end
  end
end
