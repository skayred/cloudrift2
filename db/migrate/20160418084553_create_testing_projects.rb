class CreateTestingProjects < ActiveRecord::Migration
  def change
    create_table :testing_projects do |t|
      t.string :name
    end
  end
end
