class RenameTables < ActiveRecord::Migration
  def change
    rename_table :method_options, :technique_options
    rename_table :target_methods, :test_cases
    rename_table :testing_methods, :test_techniques
    rename_table :testing_projects, :test_projects
    rename_table :testing_targets, :test_objectives

    rename_column :technique_options, :testing_method_id, :test_technique_id

    rename_column :test_cases, :testing_target_id, :test_objective_id
    rename_column :test_cases, :testing_method_id, :test_technique_id

    rename_column :test_objectives, :testing_project_id, :test_project_id
  end
end
