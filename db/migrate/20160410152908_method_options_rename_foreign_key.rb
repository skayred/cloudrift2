class MethodOptionsRenameForeignKey < ActiveRecord::Migration
  def change
    rename_column :method_options, :target_method_id, :testing_method_id
  end
end
