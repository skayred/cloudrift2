class MethodOptionsAddParameterType < ActiveRecord::Migration
  def change
    add_column :method_options, :parameter_type, :string
  end
end
