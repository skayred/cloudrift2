class PolymorphicLaunchParameters < ActiveRecord::Migration
  def change
    remove_column :launch_parameters, :test_case_id
    add_column :launch_parameters, :configurable_id, :integer
    add_column :launch_parameters, :configurable_type, :string
  end
end
