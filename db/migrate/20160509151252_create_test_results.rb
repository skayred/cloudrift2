class CreateTestResults < ActiveRecord::Migration
  def change
    enable_extension 'uuid-ossp'

    create_table :test_results, id: :uuid do |t|
      t.integer :status

      t.timestamps null: false
    end
  end
end
