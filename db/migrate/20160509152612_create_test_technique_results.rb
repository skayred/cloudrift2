class CreateTestTechniqueResults < ActiveRecord::Migration
  def change
    create_table :test_technique_results do |t|
      t.uuid :test_result_id

      t.integer :status
      t.string :report_message
      t.text :report
    end
  end
end
