class TargetMethodsAddOrdinal < ActiveRecord::Migration
  def change
    add_column :target_methods, :ordinal, :integer
  end
end
