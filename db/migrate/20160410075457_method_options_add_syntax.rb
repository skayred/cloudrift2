class MethodOptionsAddSyntax < ActiveRecord::Migration
  def change
    add_column :method_options, :possible_values, :text
  end
end
