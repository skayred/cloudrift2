class TestingMethodsAddRetryInfo < ActiveRecord::Migration
  def change
    add_column :testing_methods, :retry_count, :integer, default: 0
    add_column :testing_methods, :retried_at, :datetime
    add_column :testing_methods, :fail_reason, :text
  end
end
