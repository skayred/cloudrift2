class TestingMethodsEndpointToUrl < ActiveRecord::Migration
  def change
    rename_column :testing_methods, :endpoint, :url
    change_column :testing_methods, :url, :string
  end
end
