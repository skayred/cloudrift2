class AddProjectIdToTestingTargets < ActiveRecord::Migration
  def change
    add_column :testing_targets, :testing_project_id, :integer
  end
end
