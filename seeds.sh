#!/bin/sh

#nohup sh /usr/local/bin/run &
#sleep 10
#sudo -u postgres /usr/bin/createuser -s root
#sudo -u postgres /usr/bin/createdb cloudrift -O root
/bin/bash -l -c "bundle exec rake db:create"
/bin/bash -l -c "bundle exec rake db:migrate"
/bin/bash -l -c "bundle exec rake db:seed"